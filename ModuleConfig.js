export const LeafletsModule = false;
export const PresentationModule = false;
export const BreadCrumbsModule = false;
export const VideoCenterModule = false;
export const MediaCenterModule = false;
export const LanguageModule = true;
export const DashboardModule = false;
export const NotificationModule = false;
export const HideMenuModule = false;
export const VideoTourModule = false;
export const Temp3ScrollTextPointerModule = true;

export const ScreenSaverModule = false;