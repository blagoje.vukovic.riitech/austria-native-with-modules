import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Alert,
  StatusBar,
  AsyncStorage,
  Image,
  AppState,
  NetInfo,
  BackHandler, PanResponder, Animated
} from 'react-native';
import RNFB from 'react-native-fetch-blob';
import DeviceInfo from 'react-native-device-info';
import * as Progress from 'react-native-progress';
import md5 from 'md5';
import Routes from './src/components/Routes';
import base64 from 'base-64';
import KeepAwake from 'react-native-keep-awake';
import _ from 'lodash';
import {
  isNetworkConnected,
  syncApp,
  jsonLogic,
  thumbnailsLogic,
  initializeCheckedFiles,
  checkServer,
  contentJsonLogic,
  checkForFile,
  checkHashFiles,
  processArrayInSequence,
  zaNemaNetaDefault,
  initializeLeafletsFile,
  userTokenAndIdLogic
} from './helpers';
import OneSignal from 'react-native-onesignal';
import DownloadModal from './src/components/DownloadModal';
import Video from './src/components/Video'
import DocumentView from './src/components/Doc'
import { Actions } from 'react-native-router-flux';
import { ScreenSaverModule } from './ModuleConfig'

export default class App extends Component {

  constructor() {
    super();
    this.timeout = 0;
    console.ignoredYellowBox = [
      'Setting a timer'
    ];
  }

  state = {
    downloadedL: 0,
    downloaded: 0,
    isLoading: 1,
    visibleDownload: false,
    indeterminate: true,
    visibleDownloadError: false,
    total: 0,
    mbDone: 0,
    bonusSec: 0,
    appState: AppState.currentState,
    downloadModalUpdate: false,
    downloadModalMessage: '',
    downloadModalMessage2: '',
    downloadModalMessage3: '',
    buttons: [],
    show: false,
    videoIsActive: false
  };


  isLoading() {

    const deviceId = DeviceInfo.getUniqueID();
    let dirs = RNFB.fs.dirs;
    let defaultLanguageId;
    let defaultLanguageObject;
    let checkedFiles = {};
    let recalc = 1;
    let recalc_precision = 0.125;


    projectJsonLogic = () => {
      return new Promise((resolve, reject) => {
        jsonLogic('projectJson')
          .then(() => initializeCheckedFiles())
          .then(() => initializeLeafletsFile())
          .then(() => checkServer())
          .then(res => {
            global.server = res.config.url;
            global.languageId = global.projectJson.project.defaultLanguageId
            global.defaultLanguageObject = global.projectJson.languages.find(l => l.languageId == global.languageId);
            return Promise.resolve();
          })
          .then(() => resolve())
          .catch((err) => { console.log('catch of projectJson: ' + err); akoNemaNeta(); return reject(err) });
      })
    }

    calculateSize = (filesArr) => {
      return new Promise((resolve, reject) => {
        this.length = this.length > filesArr.length ? this.length : filesArr.length
        // console.log('=====', this.length)
        let result = 0;
        if (filesArr.length <= 0) {
          AsyncStorage.getItem('checkedFiles')
            .then(res => {
              res = JSON.parse(res);
              res.allDownloaded = true;
              AsyncStorage.setItem('checkedFiles', JSON.stringify(res));
            })
          return reject('Array is empty')
        } else {
          filesArr.forEach(element => {
            result += Number(element.size);
          });
          result = (result / 1024 / 1024).toFixed(2);
          this.setState({ visibleDownload: true, total: result });
          console.log(result)
          return resolve(result, filesArr);
        }
      })
    }


    String.prototype.capitalize = function () {
      return this.charAt(0).toUpperCase() + this.slice(1);
    }

    alertForDownload = (mb, filesArr) => {
      return new Promise((resolve, reject) => {
        if (!mb) {
          reject();
        }
        NetInfo.getConnectionInfo()
          .then((res) => {
            const speedBenchmarkFile = global.server + projectJson.project.speedBenchmarkFile;
            const pathToSpeedBenchmarkFile = dirs.DocumentDir + '/benchmark666.jpg';
            const timeBeforeDownload = Date.now();
            RNFB.config({ path: pathToSpeedBenchmarkFile }).fetch('GET', speedBenchmarkFile)
              .then((benchmarkFile) => {
                const timeAfterDownload = Date.now();
                const benchmarkTime = timeAfterDownload - timeBeforeDownload;
                RNFB.fs.readFile(benchmarkFile.path(), 'base64')
                  .then(data => {
                    const decodedData = base64.decode(data);
                    console.log('==+++++++====++++', benchmarkTime)
                    const Bytes = decodedData.length;
                    const Bits = Bytes * 8;
                    const KBitsPerSecond = Bits / (benchmarkTime < 200 ? 1000 : benchmarkTime);
                    const MBitsPerSecond = KBitsPerSecond / 1024;
                    let cellularType = res.effectiveType;
                    let warningString = res.type == 'cellular' ? 'Warning, you are on cellular ' + cellularType + ' network, this download could be charged.' : '';
                    let downloadSpeed = MBitsPerSecond;
                    global.averageSpeed = downloadSpeed;
                    let est = downloadSpeed != 0 ? (mb / downloadSpeed / 60).toFixed(0) + ' minutes ' + ((mb / downloadSpeed).toFixed(0) % 60) + ' seconds' : 'inf.';
                    if (filesArr) {
                      let b = {
                        image: { files: filesArr.filter(f => f.type == 'image'), checked: true },
                        video: { files: filesArr.filter(f => f.type == 'video'), checked: true },
                        document: { files: filesArr.filter(f => f.type == 'document'), checked: true }
                      }
                      global.dlTypes = b
                      this.setState({
                        downloadModalMessage: 'Select what you want to download: ',
                        downloadModalMessage2: global.dlTypes,
                        downloadModalMessage3: downloadSpeed,
                        downloadModalUpdate: true, isLoading: 1,
                        buttons: [{
                          naziv: 'Download',
                          akcija: (result, array, failedArray) => { this.setTimerClick((result / downloadSpeed).toFixed(0)); this.setState({ downloadModalUpdate: false, visibleDownload: true, total: result }); global.checkedFiles.failedDownloads = failedArray; return resolve(array) }
                        },
                        {
                          naziv: 'Skip',
                          akcija: () => {
                            global.checkedFiles.allDownloaded = false;
                            global.checkedFiles.failedDownloads = filesArr;
                            AsyncStorage.setItem('checkedFiles', JSON.stringify(global.checkedFiles))
                            return reject('Pritisnut reject')
                          }
                        }]
                      })
                    }

                  })
              })
              .catch(error => {
                let cellularType = res.effectiveType;
                let warningString = res.type == 'cellular' ? 'Warning, you are on cellular ' + cellularType + ' network, this download could be charged.' : '';
                let downloadSpeed = 0;
                if (res.type == 'cellular')
                  switch (res.effectiveType) {
                    case '2g':
                      downloadSpeed = 0.04 / 8.0;
                      break;
                    case '3g':
                      downloadSpeed = 6.04 / 8.0;
                      break;
                    case '4g':
                      downloadSpeed = 18.4 / 8.0;
                      break;
                  }
                if (res.type == 'wifi')
                  downloadSpeed = 23.5 / 8.0;
                global.averageSpeed = downloadSpeed;
                let est = downloadSpeed != 0 ? (mb / downloadSpeed / 60).toFixed(0) + ' minutes ' + ((mb / downloadSpeed).toFixed(0) % 60) + ' seconds' : 'inf.';
                Alert.alert(
                  'About to download ' + mb + ' MB',
                  '' + warningString + '\n' + 'Estimated time: ' + est + '.\nDo you wish to download?',
                  [
                    { text: 'OK', onPress: () => { this.setTimerClick((mb / downloadSpeed).toFixed(0)); return resolve(filesArr) } },
                    { text: 'Skip', onPress: () => reject('Pritisnut reject') }]
                )
              });
          })
      })
    }



    downloadFiles = (filesArr) => {
      console.log('usao u downloadFiles()')
      return new Promise((resolve, reject) => {
        /*let b = prepareFilesArrayIntoChunks(filesArr, 1);
        let a = b.map(chunk =>
          chunkDownload(chunk)
            .then(() => console.log('zavrsio 5'))
        );*/
        /*let a = filesArr.map(file =>
          downloadOne(file)
        );*/
        this.setState({ downloadedL: filesArr.length });
        recalc = Math.ceil(filesArr.length * recalc_precision);

        processArrayInSequence(filesArr, downloadOne)
          .then(() => console.log('All downloads finished!'))
          .then(() => filesArr.length = this.length ? global.checkedFiles.allDownloaded = true : global.checkedFiles.allDownloaded = false)
          .then(() => AsyncStorage.setItem('checkedFiles', JSON.stringify(global.checkedFiles)))
          .then(() => resolve(true))
          .catch(err => console.log('Greska kod downloadFIles(): ' + err))


      })
    }

    // TODO: dzi sredi ko za voest
    downloadOne = (file) => {
      return new Promise((resolve, reject) => {
        let t0 = Date.now();
        RNFB.config({ path: dirs.DocumentDir + '/' + file.filename }).fetch('GET', global.server + global.projectJson.project.contentDir + file.filename + '?deviceId=' + deviceId)
          .then(r => {
            if (r.info().status == 200) {
              console.log('One file downloaded at ', r.path() + ', with status code: ' + r.info().status);
              let t1 = Date.now();
              this.setState(prevState => ({ downloaded: prevState.downloaded + 1, mbDone: prevState.mbDone + (file.size) / 1024 / 1024 }));
              let time = t1 - t0;
              let sizeOne = Number(file.size) / 1024.0;
              let dlSpeed = sizeOne / time;

              if (recalc > 0 || global.averageSpeed < 0.66 * dlSpeed) {
                --recalc;
                global.averageSpeed = 0.04 * dlSpeed + (1 - 0.04) * global.averageSpeed;
                this.setState(() => ({ bonusSec: ((this.state.total - this.state.mbDone) / global.averageSpeed).toFixed(0) }));
              }
              return resolve();
            } else if (r.info().status == 404) {
              console.log('Fajl ne postoji: ' + file.fileId);
              global.checkedFiles.failedDownloads.push(file);
              AsyncStorage.setItem('checkedFiles', JSON.stringify(global.checkedFiles));
              RNFB.fs.unlink(dirs.DocumentDir + '/' + file.filename);
              return resolve();
            } else {
              console.log('Neka druga greska');
              global.checkedFiles.failedDownloads.push(file);
              AsyncStorage.setItem('checkedFiles', JSON.stringify(global.checkedFiles));
              RNFB.fs.unlink(dirs.DocumentDir + '/' + file.filename);
              return resolve();
            }

          })
          .catch((err) => {
            console.log('Fajl koruptovan: ' + file.fileId);
            global.checkedFiles.failedDownloads.push(file);
            AsyncStorage.setItem('checkedFiles', JSON.stringify(global.checkedFiles));
            RNFB.fs.unlink(dirs.DocumentDir + '/' + file.filename);
            return resolve()
          })
      })
    }

    shouldIgoToCatch = () => {
      return new Promise((resolve, reject) => {
        AsyncStorage.getItem('checkedFiles')
          .then(res => JSON.parse(res))
          .then(res => {
            if (global.isProjectJsonSame && res.allDownloaded) {
              AsyncStorage.getItem(global.defaultLanguageObject.language)
                .then(res => global.globalJson = JSON.parse(res))
                .then(() => AsyncStorage.getItem('usersJson'))
                .then(res => global.usersJson = JSON.parse(res))
                .then(() => AsyncStorage.getItem('pdfJson'))
                .then(res => { global.pdfJson = JSON.parse(res); return Promise.resolve() })
                .then(() => AsyncStorage.getItem('videosJson'))
                .then(res => { global.videosJson = JSON.parse(res); return Promise.resolve(); })
                .then(() => AsyncStorage.getItem('notificationsJson'))
                .then(res => { global.notificationsJson = JSON.parse(res); return Promise.resolve(); })
                .then(() => AsyncStorage.getItem('leafletJson'))
                .then(res => { global.leafletJson = JSON.parse(res); return Promise.resolve(); })
                .then(() => reject('reject iz shouldIgoToCatch'))
                .catch((err) => { console.log('shouldIgoToCatch: ' + err); return resolve('shouldIgoToCatch') })
            } else {
              return resolve('shouldIgoToCatch: projectJson is NEW!');
            }
          })
      })
    }

    checkLastTimeStart = () => {
      return new Promise((resolve, reject) => {
        AsyncStorage.getItem('lastTimeStart')
          .then(res => res ? Promise.resolve(res) : Promise.reject(res))
          .then(res => {
            res = Number(res);
            //console.log('Proslo startno vreme: ' + res);
            console.log(res);
            var novoVreme = Date.now();
            if (novoVreme - res >= 1000 * 60 * 60) {
              console.log('aplikacija je paljena pre VISE od sat vremena: ');
              AsyncStorage.setItem('lastTimeStart', novoVreme.toString());
              return resolve();
            } else {
              console.log('aplikacija je paljena pre MANJE od sat vremena');
              // ovde sva logika za preuzimanje jsona u lokal
              let a = ['usersJson', 'pdfJson', 'videosJson', 'notificationsJson', 'leafletJson'];
              AsyncStorage.getItem('projectJson')
                .then(res => global.projectJson = JSON.parse(res))
                .then(() => initializeCheckedFiles())
                .then(() => initializeLeafletsFile())
                .then(() => checkServer())
                .then(res => {
                  global.server = res.config.url;
                  global.languageId = global.projectJson.project.defaultLanguageId
                  global.defaultLanguageObject = global.projectJson.languages.find(l => l.languageId == global.languageId);
                  return Promise.resolve();
                })
                .then(() => AsyncStorage.getItem(global.defaultLanguageObject.language))
                .then(res => global.globalJson = JSON.parse(res))
                .then(() => AsyncStorage.multiGet(a))
                .then(res => {
                  a.map((json, i) => { global[json] = JSON.parse(res[i][1]) }); return Promise.resolve();
                })
                .then(() => reject('REJECT iz checkLastTimeStart'))
            }
          })
          .catch(res => {
            console.log('checkLastTime CATCH');
            //res = Number(res);
            let novoVreme = Date.now();
            //console.log('aaa: ' + typeof novoVreme.toString());
            AsyncStorage.setItem('lastTimeStart', novoVreme.toString());
            return resolve();
          })
      })
    }

    akoImaNeta = () => {
      checkLastTimeStart()
        .then(() => projectJsonLogic())
        .then(() => contentJsonLogic())
        .then(() => shouldIgoToCatch())
        .then(() => jsonLogic('usersJson'))
        .then(() => jsonLogic('pdfJson'))
        .then(() => jsonLogic('videosJson'))
        .then(() => jsonLogic('notificationsJson'))
        .then(() => jsonLogic('leafletJson'))
        .then(() => thumbnailsLogic())
        .catch((err) => console.log('Preskocio proveravanje jsona: ' + err))
        .then(() => checkForFile())
        .then((a) => checkHashFiles(a))
        .then((niz) => calculateSize(niz)
          .then((mb, niz2) => alertForDownload(mb, niz))
          .then((niz2) => downloadFiles(niz2))
        )
        .then(res => res ? this.setState({ isLoading: 0 }) : Promise.reject())
        .catch(err => {
          if (err == 'Postoji checkedFiles.' || err == 'Array is empty' || err == 'Pritisnut reject') {
            console.log('Catch od glavnog bloka akoImaNeta(): ' + err)
            this.setState({ isLoading: 0 })
          } else {
            console.log('Catch od glavnog bloka akoImaNeta(): ' + err)
            this.setState({ isLoading: -1 })
          }

        })
    }

    akoNemaNeta = () => {
      zaNemaNetaDefault()
        .then(() => AsyncStorage.getItem(global.defaultLanguageObject.language))
        .then((res) => res == null ? Promise.resolve({ bool: false, res: res }) : Promise.resolve({ bool: true, res: res }))
        .then(res => {
          if (!res.bool) {
            this.setState({ isLoading: -1 })
          } else {
            global.globalJson = JSON.parse(res.res);
            this.setState({ isLoading: 0 });
          }
        })
        .then(() => AsyncStorage.getItem('usersJson'))
        .then(res => global.usersJson = JSON.parse(res))
        .then(() => AsyncStorage.getItem('pdfJson'))
        .then(res => { global.pdfJson = JSON.parse(res); return Promise.resolve() })
        .then(() => AsyncStorage.getItem('notificationsJson'))
        .then(res => { global.notificationsJson = JSON.parse(res); return Promise.resolve() })
        .then(() => AsyncStorage.getItem('videosJson'))
        .then(res => { global.videosJson = JSON.parse(res); return Promise.resolve(); })
        .then(() => AsyncStorage.getItem('leafletJson'))
        .then(res => { global.leafletJson = JSON.parse(res); return Promise.resolve(); })
        .catch((err) => { console.log(err); this.setState({ isLoading: -1 }) })
    }

    fetchujGoogle = async () => {
      try {
        let result = await timeoutFetch(`https://www.google.com`, { timeout: 10000 });
        if (result) {
          let data = !!result
          return data;
        } else {
          throw error = 'nema neta'
        }
      } catch (e) {
        console.log(e)
        throw error = 'nema neta';
      }
    }

    userTokenAndIdLogic()
    .then(() => isNetworkConnected())
    .then(res => {
      if (res) {
        akoImaNeta();
      } else {
        akoNemaNeta();
      }
    })
      
    // userTokenAndIdLogic()
    // .then(() => isNetworkConnected())
    // .then(res =>
    //   fetchujGoogle()
    //     .then(konekcija => {
    //       console.log(res, ' :res ', konekcija, ' :konekcija')
    //       if (konekcija && res) {
    //         akoImaNeta()
    //       } else {
    //         akoNemaNeta()
    //       }
    //     }).catch(err => {
    //       console.log('Catch od fetchuj google, ', err)
    //       if (err == 'nema neta')
    //         akoNemaNeta()
    //     })

    // )


  } // End of isLoading()



  _handleAppStateChange = (nextAppState) => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      console.log('App has come to the foreground!');
      AsyncStorage.removeItem('Connected')
      syncApp(false);
    }
    this.setState({ appState: nextAppState });
  }

  calcProgress() {
    if (this.state.downloaded == 1) {
      this.state.indeterminate = false;
    }
    if (this.state.downloaded > 0) {
      return this.state.downloaded / this.state.downloadedL;
    }
  }

  setTimerClick = (secondsLeft) => {
    this.setState(() => ({ bonusSec: secondsLeft }));
    let interval = setInterval(() => {
      if (this.state.isLoading == 0) {
        clearInterval(interval);
      }
      this.setState((oldState) => ({ bonusSec: oldState.bonusSec - 1 }));
    }, 1000);
  }

  onReceived(notification) {
    console.log("Notification received: ", notification);
  }

  onOpened(openResult) {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
  }

  onIds(device) {
    //console.log('Device info: ', device);
  }

  componentWillMount() {
    _panResponder = {};
    _panResponder1 = {}
    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds);
    KeepAwake.activate();
    AppState.addEventListener('change', this._handleAppStateChange);
    this.isLoading();

    if (!this.state.videoIsActive && ScreenSaverModule) {
      this._panResponder = PanResponder.create({
        onStartShouldSetPanResponder: () => {
          this.resetTimer()
          console.log('RESETOVAN TAJMER')
          console.log('THIS TIMEOUT', this.timeout)
          return true
        },
        onMoveShouldSetPanResponder: () => false,
        onStartShouldSetPanResponderCapture: () => { this.resetTimer(); return false },
        onMoveShouldSetPanResponderCapture: () => false,
        onPanResponderTerminationRequest: () => true,
        onShouldBlockNativeResponder: () => false
      });
      console.log('THIS TIMEOUT', this.timeout)
      global.timer = setTimeout(() => this.setState({ show: true, videoIsActive: true, isLoading: 2 }), this.timeout != 0 ? this.timeout : 120000)
    }

    if (ScreenSaverModule) {
      this._panResponder1 = PanResponder.create({
        onStartShouldSetPanResponder: () => {
          Actions.reset('home', { lang: global.language })
          this.endScreenSaver()
          // Actions.pop()
          return true
        },
        onMoveShouldSetPanResponder: () => false,
        onStartShouldSetPanResponderCapture: () => { this.endScreenSaver(); return false },
        onMoveShouldSetPanResponderCapture: () => false,
        onPanResponderTerminationRequest: () => true,
        onShouldBlockNativeResponder: () => false,
      })
    }
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', () => {
      return true;
    })
    if (this.state.isLoading == 1 && ScreenSaverModule) {
      console.log('STATE JE U LOADINGU UBI TIMER')
      clearInterval(global.timer)
    }
  }
  resetTimer() {
    clearTimeout(global.timer)
    if (this.state.show)
      this.setState({ show: false })
    global.timer = setTimeout(() => this.setState({ show: true, videoIsActive: true, isLoading: 2 }), this.timeout)
  }

  endScreenSaver = () => {
    this.resetTimer();
    this.setState({ show: false, videoIsActive: false, isLoading: 0 })
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);
    BackHandler.removeEventListener('hardwareBackPress', () => {
      return true;
    })
  }

  findVideo = (idFajla, type) => {
    let a = global.globalJson.files.find(file => file.fileId === idFajla && file.type == type)
    if (!a) a = global.globalJson.files.find(file => file.type == type)
    return a;
  }

  componentDidUpdate(prevProps, prevState) {
    if (ScreenSaverModule) {
      if (this.state.isLoading == 0 && prevState.isLoading != this.state.isLoading) {
        console.log('STATE VISE NIJE U LOADINGU POKRENI TAJMER')
        this.resetTimer()
      }
      if (!this.state.videoIsActive && prevState.videoIsActive != this.state.videoIsActive) {
        this.resetTimer()
        console.log('USAO SAM U DID UPDATE')
      }
    }
  }


  render() {
    if (this.state.isLoading == 0) {
      if (ScreenSaverModule) {
        if (this.timeout == 0) {
          this.timeout = global.globalJson.screensaver.find(e => e.languageId == global.languageId).timeout * 1000
        }
        console.log('====== ', this.timeout)
        return (
          <Animated.View style={styles.container} {...this._panResponder.panHandlers}>
            <Routes />
          </Animated.View>
        );
      } else {
        return (
          <View style={styles.container}>
            <Routes />
          </View>
        )
      }
    } else if (this.state.show && this.state.videoIsActive && this.state.isLoading == 2) {
      return (
        <View style={{ flex: 1, width: '100%', height: '100%', zIndex: 50 }} {...this._panResponder1.panHandlers}>
          {global.globalJson.screensaver.find(e => e.languageId == global.languageId).type == 'video' && <Video videouri={'file://' + RNFB.fs.dirs.DocumentDir + '/' + this.findVideo(global.globalJson.screensaver.find(e => e.languageId == global.languageId).objectId, 'video').filename} repeat={true} endScreenSaver={this.endScreenSaver} file={true} />}
          {global.globalJson.screensaver.find(e => e.languageId == global.languageId).type == 'document' && <DocumentView docuri={'file://' + RNFB.fs.dirs.DocumentDir + '/' + this.findVideo(global.globalJson.screensaver.find(e => e.languageId == global.languageId).objectId, 'document').filename} endScreenSaver={this.endScreenSaver} file={true} />}
        </View>
      )
    } else if (this.state.isLoading == 1) {
      return (
        <View style={styles.isLoadingCont}>
          <DownloadModal
            isVisible={this.state.downloadModalUpdate}
            buttons={this.state.buttons}
            modalMessage={this.state.downloadModalMessage}
            modalMessage2={this.state.downloadModalMessage2}
            modalMessage3={this.state.downloadModalMessage3}
            close={() => this.setState({ updateModal: false })}
          />
          <Image style={styles.magnaLogo} source={require('./src/components/ico/x64/magna.png')} />

          <StatusBar barStyle="dark-content" hidden={true} />
          <View style={styles.isLoadingTextCont}>
            <Text style={styles.loadTextF}>Loading, please wait...</Text>
            {this.state.visibleDownloadError && <Text style={styles.loadText}>There seems to be corrupted download. {'\n'}Please restart the application if you see the bar below stuck.</Text>}
            {this.state.visibleDownload && <Text style={styles.loadText}>Downloaded {this.state.downloaded} of {this.state.downloadedL} files.</Text>}
            {this.state.visibleDownload && <Text style={styles.loadText}>Downloaded {Math.round(this.state.mbDone)} MB of {this.state.total} MB.</Text>}
            {this.state.visibleDownload &&
              <Text style={styles.loadText}
              >
                Remaining time: {global.averageSpeed &&
                  (this.state.bonusSec >= 60 ?
                    (Math.round(this.state.bonusSec / 60)).toFixed(0) + ' min' :
                    (this.state.bonusSec <= 0 ? 'almost done...' : (this.state.bonusSec + ' seconds'))
                  )}
              </Text>}

          </View>
          <View style={styles.progressBarCont} >
            <Progress.Bar
              style={styles.progressBar}
              indeterminate={this.state.indeterminate}
              progress={this.calcProgress()}
              color='#9E9E9E'
            />


          </View>
        </View >
      );
    }
    else if (this.state.isLoading == -1) {
      return (
        <View style={styles.isLoadingCont}>
          <StatusBar barStyle="dark-content" hidden={true} />
          <View style={styles.noNetTextCont}>
            <Text style={styles.loadText}>You are starting app for first time and you are offline.</Text>
            <Text style={styles.loadText}>We need to show some content, and for this we need to download it.</Text>
            <Text style={styles.loadText}>Please connect to internet first.</Text>
          </View>
        </View>
      );
    }

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: '100%',
    width: '100%'
  },
  isLoadingCont: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: "100%",
    height: "100%",
    backgroundColor: '#fff'
  },
  magnaLogo: {
    width: 64,
    height: 64,
    marginTop: 50
  },
  isLoadingTextCont: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: '#fff'
  },
  loadTextF: {
    alignSelf: 'center',
    color: '#9E9E9E',
    fontSize: 30,
    paddingBottom: 20
  },
  loadText: {
    color: '#9E9E9E',
    fontSize: 30,
    paddingTop: 20
  },
  progressBarCont: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff'
  },
  progressBar: {
    margin: 10
  },
  noNetTextCont: {
    flex: 3,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff'
  }
});
