import React, { Component } from 'react';
import { Platform, NetInfo, AsyncStorage, Alert, TouchableOpacity, View, Text, ScrollView, Image, Dimensions, StyleSheet } from 'react-native';
import Modal from 'react-native-modal';
import DeviceInfo from 'react-native-device-info';
import RNFB from 'react-native-fetch-blob';
import RNRestart from 'react-native-restart';
import axios from 'axios';
import VB from '../src/components/VideoBtn';
import DB from '../src/components/DocBtn';
import { Actions } from 'react-native-router-flux';
import _ from 'lodash';

const { height, width } = Dimensions.get('window');
const dirs = RNFB.fs.dirs;
const deviceId = DeviceInfo.getUniqueID();
global.deviceId = deviceId;
let fetchedJson = {};
let supportedLanguages = {};
let defaultLanguageId = 1;
let defaultLanguageObject = {};
let server = '';
let checkedFiles = {};
let pathToFile = 'file://' + RNFB.fs.dirs.DocumentDir + '/';
global.projectId = 15;
global.isProjectJsonSame = false;
global.userToken = '';
let urls = {};
let jsonToken = global.jsonToken = '4t98hrgbow'

const serverRoot = 'https://cms.sps-digital.com';

export const Dheight = Dimensions.get('window').height;
export const Dwidth = Dimensions.get('window').width;

export const userTokenAndIdLogic = () => {
    return new Promise((resolve, reject) => {
        AsyncStorage.getItem('@userToken')
            .then(res => res ? global.userToken = '&userToken=' + res : null)
            .then(() => AsyncStorage.getItem('@userId'))
            .then(res => res ? global.user = res : null)
            .then(() => {
                urls = global.urls = {
                    projectJson: serverRoot + '/?a=ajax&do=getProject&projectId=' + global.projectId + '&token='+ jsonToken +'&deviceId=' + deviceId + global.userToken,
                    usersJson: serverRoot + '/?a=ajax&do=getUsers&projectId=' + global.projectId + '&token=4t98hrgbow&deviceId=' + deviceId + global.userToken,
                    pdfJson: serverRoot + '/?a=ajax&do=getDocuments&projectId=' + global.projectId + '&token=4t98hrgbow&deviceId=' + deviceId + global.userToken,
                    videosJson: serverRoot + '/?a=ajax&do=getVideos&projectId=' + global.projectId + '&token=4t98hrgbow&deviceId=' + deviceId + global.userToken,
                    leafletJson: serverRoot + '/?a=ajax&do=getLeaflets&projectId=' + global.projectId + '&token=4t98hrgbow&deviceId=' + deviceId + global.userToken,
                    notificationsJson: serverRoot + '/?a=ajax&do=getNotifications&projectId=' + global.projectId + '&token=4t98hrgbow&deviceId=' + deviceId + global.userToken
                };
            })
            .then(() => resolve())
            .catch(e => console.log('Error iz userTokenLogic: ' + e))
    })
}

export const jsonLogic = (json) => {
    return new Promise((resolve, reject) => {
        fetch(global.urls[json])
            .then(res => res.json())
            .then(res => { fetchedJson[json] = res; return Promise.resolve() })
            .then(res => AsyncStorage.getItem(json))
            .then(res => res == null ? nePostojiJson(json) : postojiJson(json))
            .then(() => resolve())
            .catch(err => { console.log('Greska kod download-a ' + json + '. Greska: ' + err); return reject(err) })
    })
}

nePostojiJson = (json) => {
    return new Promise((resolve, reject) => {
        AsyncStorage.setItem(json, JSON.stringify(fetchedJson[json]))
            .then(() => { console.log(json + ' nije postojao i sad je skinut i smesten.'); global[json] = fetchedJson[json]; return Promise.resolve(); })
            .then(() => resolve())
            .catch(err => reject(err))
    })
}

postojiJson = (json) => {
    return new Promise((resolve, reject) => {
        AsyncStorage.getItem(json)
            .then(res => {
                global[json] = JSON.parse(res);
                if (global[json].lastChanges == fetchedJson[json].lastChanges) {
                    console.log(json + ' je isti');
                    json == 'projectJson' ? global.isProjectJsonSame = true : null;
                    return resolve(true);
                } else {
                    console.log(json + ' je razliciti');
                    global[json] = fetchedJson[json];
                    AsyncStorage.setItem(json, JSON.stringify(fetchedJson[json]))
                        .then(() => resolve(false))
                }
            })
    })
}

export const contentJsonLogic = () => {
    return new Promise((resolve, reject) => {
        initializeLanguagesFile()
            .then(() => initializeLeafletsFile())
            .then(() => setSupportedLanguage())
            .then(() => AsyncStorage.getItem(defaultLanguageObject.language))
            .then((res) => JSON.parse(res))
            .then((res) => { console.log('Defaultni jezik podesen na ' + defaultLanguageObject.language); global.globalJson = res; return Promise.resolve() })
            .then(() => resolve())
            .catch((err) => reject(err))
    })
}

initializeLanguagesFile = () => {
    return new Promise((resolve, reject) => {
        defaultLanguageId = Number(global.projectJson.project.defaultLanguageId);
        global.languageId = defaultLanguageId;
        defaultLanguageObject = global.projectJson.languages.find(l => l.languageId == defaultLanguageId);
        AsyncStorage.getItem('supportedLanguages')
            .then((res) => JSON.parse(res))
            .then((res) => {
                if (res == null) {
                    supportedLanguages = { currentlySupportedLanguages: [] };
                    supportedLanguages.currentlySupportedLanguages.push(defaultLanguageObject);

                } else {
                    supportedLanguages = res;
                    supportedLanguages.currentlySupportedLanguages.push(defaultLanguageObject);
                    supportedLanguages.currentlySupportedLanguages = _.uniqBy(supportedLanguages.currentlySupportedLanguages, 'languageId');
                }
                return Promise.resolve();
            })
            .then(() => JSON.stringify(supportedLanguages))
            .then((res) => AsyncStorage.setItem('supportedLanguages', res))
            .then(() => resolve())
    })
}

export const initializeLeafletsFile = () => {
    return new Promise((resolve, reject) => {
        AsyncStorage.getItem('leaflets')
            .then(res => JSON.parse(res))
            .then(res => {
                let leaflets;
                if (res == null) {
                    leaflets = global.leaflets = { userId: false, sendToEmail: false, sendToPerson: false, message: false, pages: [], fullPages: [] }
                } else {
                    leaflets = global.leaflets = res;
                }
                return Promise.resolve(leaflets);
            })
            .then(res => AsyncStorage.setItem('leaflets', JSON.stringify(res)))
            .then(() => resolve());
    })
}

setSupportedLanguage = () => {
    return new Promise((resolve, reject) => {
        AsyncStorage.getItem('supportedLanguages')
            .then((res) => JSON.parse(res))
            .then((res) => {
                let a = global.projectJson.languages.map(l => checkContentForLang(l))
                Promise.all(a)
                    .then(() => resolve())
            })
            .catch(() => resolve('Neka greska kod setSupportedLang'));
    })
}

checkContentForLang = (lang) => {
    return new Promise((resolve, reject) => {
        AsyncStorage.getItem(lang.language)
            .then(res => JSON.parse(res))
            .then(res => res == null ? Promise.resolve({ bool: false, res: res }) : Promise.resolve({ bool: true, res: res }))
            .then(res => {
                if (res.bool) { // ako postoji json za jezik
                    console.log('Postoji content za ' + lang.language);
                    let urlZaJezik = global.server + '?a=ajax&do=getContentByLanguage&languageId=' + lang.languageId + '&projectId=' + global.projectId + '&token=4t98hrgbow&deviceId=' + deviceId + global.userToken;
                    let localLastChanges = res.res.lastChanges;
                    fetch(urlZaJezik)
                        .then(res => res.json())
                        .then(res => {
                            if (res.lastChanges == localLastChanges) {
                                console.log('LASTCHANGES ' + res.lastChanges, localLastChanges)
                                console.log('Content JSON za jezik ' + lang.language + ' je isti kao i u lokalu i ne treba ga menjati.');
                                return resolve();
                            } else {
                                console.log('Content JSON za jezik ' + lang.language + ' je RAZLICITI i treba ga opet skinuti, smestiti i naci fajlove.');
                                AsyncStorage.setItem(lang.language, JSON.stringify(res))
                                    .then(() => AsyncStorage.getItem('checkedFiles'))
                                    .then(res => JSON.parse(res))
                                    .then(res => { res && res.allDownloaded ? res.allDownloaded = false : null; return Promise.resolve(res); })
                                    .then((res) => AsyncStorage.setItem('checkedFiles', JSON.stringify(res)))
                                    .then(() => resolve())
                            }
                        })

                } else { // ako ne postoji json za jezik
                    console.log('Ne postoji contentJSON za jezik ' + lang.language + ' i treba proveriti da li se treba skinuti');
                    AsyncStorage.getItem('supportedLanguages')
                        .then((supp) => JSON.parse(supp))
                        .then((supp) => {
                            let urlZaJezik = global.server + '?a=ajax&do=getContentByLanguage&languageId=' + lang.languageId + '&projectId=' + global.projectId + '&token=4t98hrgbow&deviceId=' + deviceId + global.userToken;

                            let ind = supp.currentlySupportedLanguages.findIndex(l => l.languageId == lang.languageId);

                            if (ind >= 0) {
                                console.log('Content JSON za jezik ' + lang.language + ' se treba skinuti.')
                                fetch(urlZaJezik)
                                    .then(res => res.json())
                                    .then(res => AsyncStorage.setItem(lang.language, JSON.stringify(res)))
                                    .then(() => AsyncStorage.getItem('checkedFiles'))
                                    .then(res => JSON.parse(res))
                                    .then(res => { res && res.allDownloaded ? res.allDownloaded = false : null; return Promise.resolve(res); })
                                    .then((res) => AsyncStorage.setItem('checkedFiles', JSON.stringify(res)))
                                    .then(() => resolve())

                            } else {
                                console.log('Jezik ' + lang.language + ' se ne treba skinuti');
                                return resolve();
                            }

                        })
                }
            })
            .catch((err) => console.log('Neka greska za jezik ' + lang.language + '.\n ' + err))
    })
}

export const checkForFile = () => {
    return new Promise((resolve, reject) => {
        AsyncStorage.getItem('checkedFiles')
            .then(res => {
                if (res == null) {
                    return resolve([]);
                } else {
                    res = JSON.parse(res);
                    global.checkedFiles = res;
                    if (res.failedDownloads.length > 0) {
                        return resolve(res.failedDownloads);
                    } else if (res.allDownloaded) {
                        return reject('Postoji checkedFiles.')
                    } else {
                        return resolve([]);
                    }
                }
            })
    })
}

export const checkHashFiles = (pocetni) => {
    console.log('usao u hash files()');
    return new Promise((resolve, reject) => {
        let downloadStage = pocetni;
        //console.log('pocetni niz: ' + pocetni);
        global.checkedFiles.failedDownloads = [];
        AsyncStorage.getItem('supportedLanguages')
            .then(res => JSON.parse(res))
            .then(res => {
                let a = res.currentlySupportedLanguages.map(l => {
                    return new Promise((resolve, reject) => {
                        AsyncStorage.getItem(l.language)
                            .then(res => JSON.parse(res))
                            .then(res => {
                                if (!res.files) { return reject('undefined') }
                                let b = res.files.map(file => {
                                    return new Promise((resolve, reject) => {
                                        RNFB.fs.exists(dirs.DocumentDir + '/' + file.filename)
                                            .then(res => {
                                                if (!res) { /* && md5(dirs.DocumentDir + '/' + file.fileId + '.' + file.ext)  != file.hash*/
                                                    downloadStage.push(file);
                                                }
                                                return Promise.resolve();
                                            })
                                            .then(() => resolve())
                                    })
                                })
                                Promise.all(b)
                                    .then(() => resolve())
                            })
                    })
                })
                Promise.all(a)
                    .then(() => { downloadStage = _.uniqBy(downloadStage, 'filename'); return Promise.resolve(); })
                    .then(() => resolve(downloadStage))
                    .catch(err => { if (err == 'undefined') { return resolve(downloadStage) } console.log('Greska kod checkHashFiles()' + err) })

            })
    })
}

export const zaNemaNetaDefault = () => {
    return new Promise((resolve, reject) => {
        global.server = serverRoot;
        AsyncStorage.getItem('projectJson')
            .then(res => res != null ? res : Promise.reject('Ne postoji project JSON u lokalu'))
            .then(res => JSON.parse(res))
            .then(res => {
                global.projectJson = res;
                defaultLanguageId = Number(global.projectJson.project.defaultLanguageId);
                !global.languageId ? global.languageId = defaultLanguageId : false;
                global.defaultLanguageObject = global.projectJson.languages.find(l => l.languageId == defaultLanguageId);
                return Promise.resolve();
            })
            .then(() => initializeLeafletsFile())
            .then(() => resolve())
            .catch(err => reject(err));
    })
}

export const initializeCheckedFiles = () => {
    return new Promise((resolve, reject) => {
        AsyncStorage.getItem('checkedFiles')
            .then(res => {
                if (res == null) {
                    AsyncStorage.setItem('checkedFiles', JSON.stringify({ failedDownloads: [], allDownloaded: false }))
                        .then(() => resolve())
                } else {
                    return resolve();
                }
            })
    })
}

export const checkServer = () => {
    let a = global.projectJson.project.servers.map(server =>
        axios.get(server)
    );
    return Promise.resolve(a[0]);
}

export const thumbnailsLogic = () => {
    return new Promise((resolve, reject) => {
        RNFB.fs.exists(RNFB.fs.dirs.DocumentDir + '/videoThumbs/')
            .then(res => !res ? nePostojeThumbsi() : postojeThumbsi())
            .then(() => resolve())
            .catch(err => reject(err))
    })
}

nePostojeThumbsi = () => {
    return new Promise((resolve, reject) => {
        let dupFreeVideos = _.uniqBy(global.videosJson.videos, 'thumbnail');
        let dl = dupFreeVideos.map(video =>
            RNFB.config({ path: dirs.DocumentDir + '/videoThumbs/' + video.thumbnail }).fetch('GET', global.server + video.thumbnail)
                .then(r => console.log('One thumb downloaded to: ' + r.path()))
        )
        return resolve()
        let dupFreeDocs = global.pdfJson.documents.map(d => d.files);
        dupFreeDocs = _.flatten(dupFreeDocs);
        dupFreeDocs = _.uniqBy(dupFreeDocs, 'thumbnail');
        dupFreeDocs = dupFreeDocs.filter(d => !!d);
        console.log(dupFreeDocs)

        let pdfThumb = dupFreeDocs.map(t => {
            if (t.thumbnail) {
                return RNFB.config({ path: dirs.DocumentDir + '/videoThumbs/' + t.thumbnail }).fetch('GET', global.server + t.thumbnail)
                    .then(r => console.log('One thumb downloaded to: ' + r.path()))
            } else {
                return Promise.resolve()
            }
        }
        )
        let all = dl.concat(pdfThumb)

        Promise.all(all)
            .then(() => resolve())
            .catch(err => console.log(err))
        // Promise.all([dl, pdfThumb])
        //     .then(() => resolve())
        //     .catch(err => console.log(err))
    })
}

postojeThumbsi = () => {
    return new Promise((resolve, reject) => {
        let dupFreeVideos = _.uniqBy(global.videosJson.videos, 'thumbnail');
        let a = dupFreeVideos.map(video => {
            RNFB.fs.exists(dirs.DocumentDir + '/videoThumbs/' + video.thumbnail)
                .then(res => {
                    if (!res) {
                        return RNFB.config({ path: dirs.DocumentDir + '/videoThumbs/' + video.thumbnail }).fetch('GET', global.server + video.thumbnail)
                            .then(() => { console.log('Ovaj thumbnail ne postoji potreban je ponovni download: ' + video.thumbnail); return Promise.resolve() })
                    }
                })
        })

        let dupFreeDocs = global.pdfJson.documents.map(d => d.files);
        dupFreeDocs = _.flatten(dupFreeDocs);
        dupFreeDocs = _.uniqBy(dupFreeDocs, 'thumbnail');

        let b = dupFreeDocs.map(t => {
            RNFB.fs.exists(dirs.DocumentDir + '/videoThumbs/' + t.thumbnail)
                .then(res => {
                    if (!res) {
                        return RNFB.config({ path: dirs.DocumentDir + '/videoThumbs/' + t.thumbnail }).fetch('GET', global.server + t.thumbnail)
                            .then(() => { console.log('Ovaj thumbnail ne postoji potreban je ponovni download: ' + t.thumbnail); return Promise.resolve() })
                    }
                })
        })

        Promise.all([a, b])
            .then(() => resolve())
            .catch(err => { console.log(err); return reject() })
    })
}

export const isNetworkConnected = () => {
    if (Platform.OS === 'ios') {
        return new Promise(resolve => {
            let handleFirstConnectivityChangeIOS = isNetworkConnected => {
                console.log(isNetworkConnected)
                NetInfo.isConnected.removeEventListener('connectionChange', handleFirstConnectivityChangeIOS);
                return resolve(isNetworkConnected);
            };
            NetInfo.isConnected.addEventListener('connectionChange', handleFirstConnectivityChangeIOS);
        });
    }
    return NetInfo.isConnected.fetch();
}

export const syncApp = (showAlerts) => {
    return new Promise((resolve, reject) => {
        isNetworkConnected()
            .then(res => {
                if (res) {
                    AsyncStorage.getItem('checkedFiles')
                        .then((res) => JSON.parse(res))
                        .then(fajlic => {
                            fetch(global.urls.projectJson)
                                .then(res => res.json())
                                .then(res => {
                                    let neSkinutiFajlovi = fajlic.failedDownloads.length > 0 ? 'There seems to be ' + fajlic.failedDownloads.length + ' missing files. Try syncing the app. \nIf this problem persists, that means files are missing from the server. \nContact your admin to fix it.' : 'Seems everything is OK. If you want you can sync application anyway.';
                                    if (res.lastChanges == global.projectJson.lastChanges) {
                                        showAlerts && Alert.alert('UP TO DATE!', neSkinutiFajlovi, [{ text: 'Sync', onPress: () => { AsyncStorage.removeItem('lastTimeStart').then(() => RNRestart.Restart()) } }, { text: 'Cancel', onPress: () => { } }])
                                    } else {
                                        Alert.alert('There seems to be update!', 'Do you wish to sync?', [{ text: 'Sync', onPress: () => { AsyncStorage.removeItem('lastTimeStart').then(() => RNRestart.Restart()) } }, { text: 'Cancel', onPress: () => { } }]);
                                    }
                                })
                                .catch(() => { showAlerts && Alert.alert('Error', 'Something went wrong. Please check your internet connection, restart the app, or try again later.', [{ text: 'OK', onPress: () => { } }]); })
                                .then(() => resolve())
                        })
                } else {
                    showAlerts && Alert.alert('Offline', 'You seem to be offline.', [{ text: 'OK', onPress: () => { } }]);
                    return resolve();
                }
            })

    })
}

export const processArrayInSequence = (array, fn) => {
    var index = 0;
    return new Promise((resolve, reject) => {
        function next() {
            if (index < array.length) {
                fn(array[index++]).then(next, reject);
            } else {
                resolve();
            }
        }
        next();
    })
}

export const findPageObjectById = (b) => {
    return [global.globalJson.pages.find(p => p.menuId == b)];
}

export const findMenuObjectById = (menuIdS) => {
    // let menus = global.globalJson.menuTrees[global.language].menuTree;
    let found = global.globalJson.menus[global.language].menu.find(m => m.menuId == menuIdS)



    // for (let i = 0; i < menus.length; i++) {
    //     if (menus[i].menuId == menuIdS) { found = menus[i]; console.log('OVO JE FOUND U IF', found); break; }
    //     else {
    //         if (menus[i].children)
    //             for (let j = 0; j < menus[i].children.length; j++) {
    //                 if (menus[i].children[j].menuId == menuIdS) { found = menus[i].children[j]; console.log('OVO JE FOUND U IF ELSE', found); break; }
    //                 else {
    //                     if (menus[i].children[j].children) {
    //                         for (let k = 0; k < menus[i].children[j].children.length; k++) {
    //                             if (menus[i].children[j].children[k].menuId == menuIdS) { found = menus[i].children[j].children[k];console.log('OVO JE FOUND U IF ELSE ELSE', found); break; }
    //                         }
    //                     }
    //                 }
    //             }
    //     }
    // }
    return found;
}


export const findMenu1Selected = (m) => {
    let index;
    if (m) {
        if (m.parentId == 0) {
            return index = global.globalJson.menuTrees[global.language].menuTree.findIndex(cale => cale.menuId == m.menuId);
        }
        else {
            let a = global.globalJson.menus[global.language].menu.find(x => x.menuId == m.parentId);
            return findMenu1Selected(a);
        }
    }
}

export const findMenu = (menuIdS) => {
    let menus = global.globalJson.menus[global.language].menu;
    return menus.find(m => m.menuId == menuIdS);
}

export const renderVB = (arr, func) => {
    if (arr.length == 0) {
        return null;
    }
    else if (arr.length == 1) {
        return <VB videouri={pathToFile + arr[0].filename} />;
    } else {
        return <TouchableOpacity onPress={() => func()}><VB disabled={true} /></TouchableOpacity>;
    }
}

export const renderDB = (arr, func) => {
    if (arr.length == 0) {
        return null;
    }
    else if (arr.length == 1) {
        return <DB documenturi={pathToFile + arr[0].filename} />;
    } else {
        return <TouchableOpacity onPress={() => func()}><DB disabled={true} /></TouchableOpacity>;
    }
}

export const renderModalforMultipleFiles = (what, arr, isVisible, func) => {
    return (
        <Modal
            isVisible={!!isVisible}
            transparent={true}
            animationType={'fade'}
            onBackdropPress={() => func()}
            onBackButtonPress={() => func()}>

            <View style={styles.modalMainCont}>
                <View style={styles.cancelBtnCont}>
                    <TouchableOpacity onPress={() => func()} style={styles.cancelBtn}>
                        <View style={{ width: '92%' }}></View>
                        <View style={{ width: '3%' }}><Image style={styles.cancelIcon} source={require('../src/components/images/cancel.png')} /></View>
                    </TouchableOpacity>
                </View>
                <ScrollView showsVerticalScrollIndicator contentContainerStyle={styles.listContent} style={styles.listStyle}>
                    {this.renderListOfFiles(what, arr, func)}
                </ScrollView>
            </View>

        </Modal>
    );
}

renderListOfFiles = (what, arr, func) => {
    let whaturi = what == 'videos' ? 'videouri' : 'docuri';
    let whatView = what == 'videos' ? 'VideoView' : 'DocumentView';
    return arr.map((f, i) => {
        return (
            <TouchableOpacity style={styles.singleItem} key={i} onPress={() => { Actions[whatView]({ [whaturi]: pathToFile + f.filename }); func() }} >
                <View style={styles.singleItemInnerCont}>
                    {what == 'documents' && <Image style={styles.singleItemImg} source={{ uri: pathToFile + 'videoThumbs/' + f.thumbnail }} />}
                    {what == 'videos' && <Image style={styles.singleItemImg} source={{ uri: pathToFile + 'videoThumbs/' + f.thumbnail }} />}
                    <Text numberOfLines={1} style={styles.itemTitle}>{f.title}</Text>
                </View>
            </TouchableOpacity>
        );
    })

}


global.leaflets = {
    userId: false,
    sendToEmail: false,
    sendToPerson: false,
    message: false,
    pages: [],
    fullPages: []
}

const styles = StyleSheet.create({
    modalMainCont: {
        flex: 1,
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center'
    },
    cancelBtnCont: {
        width: '100%',
        margin: 5
    },
    cancelBtn: {
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    cancelIcon: {
        width: 32,
        height: 32
    },
    listContent: {
        flexGrow: 1,
        flexDirection: 'row',
        alignContent: 'center',
        flexWrap: 'wrap',
        justifyContent: 'flex-start'
    },
    listStyle: {
        width: '100%',
        alignContent: 'center'
    },
    singleItem: {
        height: 260,
        width: '32%'
    },
    singleItemInnerCont: {
        minWidth: '26%',
        margin: '3%',
        alignItems: 'flex-start',
        justifyContent: 'center',
        flexDirection: 'column'
    },
    singleItemImg: {
        width: '100%',
        height: 200
    },
    itemTitle: {
        fontSize: 15,
        color: '#fff',
        paddingTop: 15,
        paddingBottom: 15,
        textAlign: 'left'
    }
});
