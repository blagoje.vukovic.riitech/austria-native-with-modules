import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { renderModalforMultipleFiles } from '../../helpers';

export default class VideoTour extends Component {

    render() {
        return (
            <TouchableOpacity
                onPress={() => this.props.open()}
                style={{ marginTop: -22, backgroundColor: '#CFCFCF', opacity: 0.9, height: 150, width: 200, position: 'absolute', top: 0, right: 0, zIndex: 1,  borderBottomLeftRadius: 200 }}
            >
                <View style={{ height: '100%', flexDirection: 'column', justifyContent: 'flex-start', alignItems:'flex-end', marginTop: 30, marginRight: 10 }}>
                <View style={{width: '70%', height: '60%', justifyContent:'center', alignItems: 'center'}}>
                    <Image style={{ width: 60, height: 60}} source={require('./images/play.png')}  />
                    <Text style={{ color: '#fff', fontSize: 18 }}>PLAY VIDEO</Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}
