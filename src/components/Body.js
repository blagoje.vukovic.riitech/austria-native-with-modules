import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, TouchableOpacity, Image, ImageBackground } from 'react-native';
import FullImage from './temp1_fullimage';
import ImageButtons from './temp2_imagebuttons';
import TextImage from './temp3_textimage';
import FullText from './temp4_fulltext';
import Swiper from 'react-native-swiper';
import RNFB from 'react-native-fetch-blob';
import HotspotImage from './temp5_imageHotspot';
import Canvas from './temp6_canvas';

class Body extends Component {
    constructor() {
        super();
        this.isTemp6 = false;
    }

    state = { i: 0, start: false }

    temp6prop = (bool) => {
        this.isTemp6 = bool
    }

    filterBody() {
        const pathToFiles = `file://${RNFB.fs.dirs.DocumentDir}/`
        //const pathToFiles = RNFB.fs.dirs.DocumentDir + '/';
        return this.props.pages.map(page => {

            let title = page.title;
            let subtitle = page.subtitle;
            let text = page.text;
            let files = [];
            // promeniti naziv prema JSON-u i proveriti da li ih ima
            let hotspot = [];

            if (page.files) {
                files = page.files.map(file => {
                    return pathToFiles + file.filename;
                })
            }

            switch (page.templateId) {
                case '1':
                    return <FullImage changed={this.state.i} fromHome={this.props.fromHome} page={page} key={page.pageId} files={files} />
                    break;

                case '2':
                    return <ImageButtons changed={this.state.i} fromHome={this.props.fromHome} page={page} key={page.pageId} templateTitle={title} subtitle={subtitle} files={files} />
                    break;

                case '3':
                    return <TextImage changed={this.state.i} fromHome={this.props.fromHome} page={page} key={page.pageId} templateTitle={title} subtitle={subtitle} files={files} text={text} />
                    break;

                case '4':
                    return <FullText changed={this.state.i} fromHome={this.props.fromHome} page={page} key={page.pageId} subtitle={subtitle} templateTitle={title} text={text} files={files} />
                    break;

                case '5':
                    return <HotspotImage changed={this.state.i} fromHome={this.props.fromHome} page={page} key={page.pageId} />
                    break;

                case '6':
                    return <Canvas changed={this.state.i} temp6prop={this.temp6prop} start={this.state.start} fromHome={this.props.fromHome} page={page} key={page.pageId} files={files} />
                    break;

                default:
                    console.log('WTF?!');
            }
        })
    }



    indexChanged = (i) => {
        this.setState({ i: i });
        this.props.indexChaged(i);
    }

    render() {

        return (

            <View style={styles.bodyCont}>

                <Swiper
                    onScrollBeginDrag={(e, s, c) => { this.setState({ start: true }) }}
                    onTouchEnd={(e, s, c) => { this.setState({ start: false }) }}
                    onIndexChanged={(i) => this.indexChanged(i)}
                    loadMinimal={true}
                    index={this.props.indexStart}
                    loop={false}
                    paginationStyle={styles.pagginationStyle}
                    removeClippedSubviews={true}
                    dot={<View><ImageBackground style={styles.dotstyle} source={require('./ico/icon.png')}></ImageBackground></View>}
                    activeDot={<View style={styles.activedotstyle}></View>}
                >
                    {this.filterBody()}
                </Swiper>

            </View>
        );

    }
}

const styles = StyleSheet.create({
    bodyCont: {
        backgroundColor: 'white',
        width: '100%',
        height: '93%',
    },
    pagginationStyle: {
        bottom: 0,
    },
    dotstyle: {
        width: 14,
        height: 14,
        borderRadius: 7,
        margin: 3,
    },
    activedotstyle: {
        backgroundColor: 'white',
        borderWidth: 1.5,
        borderColor: '#CFCFCF',
        width: 14,
        height: 14,
        borderRadius: 7,
        margin: 3,
    },
});

export default Body;