import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, Image, Text, Settings, TouchableWithoutFeedback, Button } from 'react-native';
import Header from './Header';
import Footer from './Footer';
import MenuList from './MenuList';
import Body from './Body';
import SettingsComponent from './Settings';
import Languages from './Languages';
import NotificationComponent from './Notification';
import Search from './Search';
import PdfList from './PdfList';
import Dashboard from './Dashboard';
import PresentationNewComponent from './PresentationNew';
import Breadcrumbs from './Breadcrumbs';
import Leaflets from './Leaflets';
import Videos from './Videos';
import LeafletButtons from './LeafletButton';
import SlidingUpPanel from 'rn-sliding-up-panel';
import GestureRecognizer, { swipeDirections } from 'react-native-swipe-gestures';
import { HideMenuModule } from '../../ModuleConfig'

class HBF extends Component {

    state = {
        visibleMenu: false,
        visibleSearch: false,
        visiblelanguage: false,
        visiblesettings: false,
        visibleVideos: false,
        visiblePdf: false,
        languangeId: 0,
        visiblenotification: false,
        visibledashboard: false,
        visiblepresentation: this.props.visiblepresentation || false,
        visiblebreadcrumbs: false,
        visibleleaflets: false,
        visiblevideos: false,
        whatIsOpen: this.props.whatIsOpen || '',
        currentIndex: this.props.indexStart ? this.props.indexStart : 0,
        hidemenu: false
    }

    hideMenu = () => {
        setTimeout(() => {
            this.setState({ hidemenu: true })
        }, 10000)
    }

    componentDidMount() {
        if(HideMenuModule)
        this.hideMenu();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.visiblepresentation == true) {
            this.setState({ visiblepresentation: true })
        }
        if (nextProps.visiblepresentation == false) {
            this.setState({ visiblepresentation: false })
        }
        this.forceUpdate();
    }
    onSwipeUp(gestureState) {
        this.setState({ myText: 'You swiped up!' });
    }


    render() {
        return (

            <View>
                {this.state.visible && <TouchableWithoutFeedback onPress={() => this._panel.transitionTo(0)} ><View style={styles.closePanelBtn}></View></TouchableWithoutFeedback>}
                <Header
                    backEnabled={this.props.backEnabled}
                    whatIsOpen={this.state.whatIsOpen}
                    home={this.props.fromHome}
                    title={this.props.presentationTitle || this.props.from.title}
                    onPressLang={() => { this.state.visiblelanguage ? this.setState({ visiblelanguage: false, whatIsOpen: '' }) : this.setState({ visiblelanguage: true, visibleMenu: false, visibleSearch: false, visiblesettings: false, visiblevideos: false, visiblePdf: false, visiblenotification: false, visibledashboard: false, visiblepresentation: false, visiblepresentationsort: false, visiblebreadcrumbs: false, visibleleaflets: false, whatIsOpen: 'Language' }) }}
                    onPressSearch={() => { this.state.visibleSearch ? this.setState({ visibleSearch: false, whatIsOpen: '' }) : this.setState({ visibleSearch: true, visibleMenu: false, visiblelanguage: false, visiblesettings: false, visiblevideos: false, visiblePdf: false, visiblenotification: false, visibledashboard: false, visiblepresentation: false, visiblepresentationsort: false, visiblebreadcrumbs: false, visibleleaflets: false, whatIsOpen: 'Search' }) }}
                    onPressSettings={() => { this.state.visiblesettings ? this.setState({ visiblesettings: false, whatIsOpen: '' }) : this.setState({ visiblesettings: true, visibleMenu: false, visibleSearch: false, visiblelanguage: false, visiblevideos: false, visiblePdf: false, visiblenotification: false, visibledashboard: false, visiblepresentation: false, visiblepresentationsort: false, visiblebreadcrumbs: false, visibleleaflets: false, whatIsOpen: 'Settings' }) }}
                    onPressVideos={() => { this.state.visiblevideos ? this.setState({ visiblevideos: false, whatIsOpen: '' }) : this.setState({ visiblevideos: true, visiblesettings: false, visibleMenu: false, visibleSearch: false, visiblelanguage: false, visiblePdf: false, visiblenotification: false, visibledashboard: false, visiblepresentation: false, visiblepresentationsort: false, visiblebreadcrumbs: false, visibleleaflets: false, whatIsOpen: 'Videos' }) }}
                    onPressMediaCenter={() => { this.state.visiblePdf ? this.setState({ visiblePdf: false, whatIsOpen: '' }) : this.setState({ visiblePdf: true, visiblelanguage: false, visibleMenu: false, visibleSearch: false, visiblesettings: false, visiblevideos: false, visiblenotification: false, visibledashboard: false, visiblepresentation: false, visiblepresentationsort: false, visiblebreadcrumbs: false, visibleleaflets: false, whatIsOpen: 'Mediacenter' }) }}
                    onPressNotification={() => { this.state.visiblenotification ? this.setState({ visiblenotification: false, whatIsOpen: '' }) : this.setState({ visiblenotification: true, visibleMenu: false, visibleSearch: false, visiblelanguage: false, visiblesettings: false, visiblePdf: false, visiblevideos: false, visibledashboard: false, visiblepresentation: false, visiblepresentationsort: false, visiblebreadcrumbs: false, visibleleaflets: false, whatIsOpen: 'Notifications' }) }}
                    onPressDashboard={() => { this.state.visibledashboard ? this.setState({ visibledashboard: false, whatIsOpen: '' }) : this.setState({ visibledashboard: true, visibleMenu: false, visibleSearch: false, visiblelanguage: false, visiblevideos: false, visiblePdf: false, visiblenotification: false, visiblesettings: false, visiblepresentation: false, visiblepresentationsort: false, visiblebreadcrumbs: false, visibleleaflets: false, whatIsOpen: 'Dashboard' }) }}
                    onPressPresentation={() => { this.state.visiblepresentation ? this.setState({ visiblepresentation: false, whatIsOpen: '' }) : this.setState({ visiblepresentation: true, visibleMenu: false, visibleSearch: false, visiblelanguage: false, visiblevideos: false, visiblePdf: false, visiblenotification: false, visiblesettings: false, visiblepresentationsort: false, visibledashboard: false, visiblebreadcrumbs: false, visibleleaflets: false, whatIsOpen: 'Presentation' }) }}
                    onPressBreadcrumbs={() => { this.state.visiblebreadcrumbs ? this.setState({ visiblebreadcrumbs: false, whatIsOpen: '' }) : this.setState({ visiblebreadcrumbs: true, visibleMenu: false, visibleSearch: false, visiblelanguage: false, visiblevideos: false, visiblePdf: false, visiblenotification: false, visiblesettings: false, visiblepresentation: false, visiblepresentationsort: false, visibleleaflets: false, whatIsOpen: 'Breadcrumbs' }) }}
                    onPressLeaflets={() => { this.state.visibleleaflets ? this.setState({ visibleleaflets: false, whatIsOpen: '' }) : this.setState({ visibleleaflets: true, visibleMenu: false, visibleSearch: false, visiblelanguage: false, visiblevideos: false, visiblePdf: false, visiblenotification: false, visiblesettings: false, visiblepresentation: false, visiblepresentationsort: false, visiblebreadcrumbs: false, whatIsOpen: 'Leaflets' }) }}
                    onPressHome={() => { this.setState({ visiblebreadcrumbs: false, visibleMenu: false, visibleSearch: false, visiblelanguage: false, visiblevideos: false, visiblePdf: false, visiblenotification: false, visiblesettings: false, visiblepresentation: false, visiblepresentationsort: false, visibleleaflets: false, whatIsOpen: '' }) }}
                //  onPressPresentation={() => { this.state.visiblepresentationsort ? this.setState({ visiblepresentationsort: false }) : this.setState({ visiblepresentationsort: true, visibleMenu: false, visibleSearch: false, visiblelanguage: false, visibleVideos: false, visiblePdf: false, visiblenotification: false, visiblesettings: false, visibledashboard: false, visiblepresentation: false }) }}
                />


                {this.state.visiblelanguage &&
                    <Languages />
                }
                {this.state.visibleSearch &&
                    <Search />
                }
                {this.state.visiblesettings &&
                    <SettingsComponent />
                }
                {this.state.visiblenotification &&
                    <NotificationComponent close={() => this.setState({ visiblenotification: false, whatIsOpen: '' })} />
                }


                {this.state.visiblePdf &&
                    <PdfList />
                }
                {this.state.visibledashboard &&
                    <Dashboard />
                }
                {this.state.visiblebreadcrumbs &&
                    <Breadcrumbs from={this.props.from.menuId} index={this.state.currentIndex} pages={this.props.filtered} />
                }
                {this.state.visibleleaflets &&
                    <Leaflets />
                }
                {this.state.visiblevideos &&
                    <Videos />
                }

                {
                    this.state.visiblepresentation &&
                    <PresentationNewComponent
                        closePresentation={() => { this.setState({ visiblepresentation: false }) }}
                        lookingAt={this.props.pagePresentation}
                        fromHbf={true} />
                }

                {/* {
                    this.state.visiblepresentationsort &&
                    <PresentationSortComponent />

                }  */}

                {this.state.isModalVisibleForgotPswd &&
                    <ForgotPassword
                        ForgotPswdBack={() => { this.setState({ isModalVisibleForgotPswd: !this.state.isModalVisibleForgotPswd, isModalVisible: !this.state.isModalVisible }) }}
                        ForgotPswdDrop={() => { this.setState({ isModalVisibleForgotPswd: !this.state.isModalVisibleForgotPswd }) }} />
                }

                <Body indexChaged={(i) => { this.setState({ currentIndex: i }) }} indexStart={this.props.indexStart ? this.props.indexStart : 0} fromHome={this.props.fromHome} style={{ bottom: 10 }} pages={this.props.filtered} />

                {/* <LeafletButtons
                    onPressPresentation={() => { this.setState({ visiblepresentation: !this.state.visiblepresentation }) }}
                /> */}

                {!this.state.visibleMenu && !this.state.hidemenu &&
                    <GestureRecognizer
                        onSwipeUp={(state) => this.setState({ visible: true })}  >
                        <View style={styles.menuBtnCont}>
                            <View style={styles.menuBtnInnercont}>
                                <TouchableOpacity onPress={() => this.setState({ visible: true })} style={styles.menuBtn}>
                                    <Image style={styles.ico} source={require('./ico/32/hamburger_menue.png')} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </GestureRecognizer>
                }
                <SlidingUpPanel
                    showBackdrop={true}
                    allowDragging={false}
                    ref={c => this._panel = c}
                    visible={this.state.visible}
                    onRequestClose={() => this.setState({ visible: false })}>
                    <View style={styles.slidingUpPanelCont}>
                        <View style={styles.slidingUpPanelInnerCont}>
                            <GestureRecognizer
                                onSwipeDown={(state) => this._panel.transitionTo(0)}>
                                <TouchableOpacity onPress={() => this._panel.transitionTo(0)} style={{ width: '100%', height: '100%' }}>
                                    <Image style={styles.ico} source={require('./ico/32/hamburger_menue.png')} />
                                </TouchableOpacity>
                            </GestureRecognizer>
                        </View>
                        <View style={{ height: '100%', }}>
                            {/* <GestureRecognizer */}
                            {/* onSwipeDown={(state) => this._panel.transitionTo(0)}> */}
                            <MenuList selected={this.props.selected} data={global.globalJson} from={this.props.from.menuId} />
                            {/* </GestureRecognizer> */}
                        </View>
                    </View>
                </SlidingUpPanel>

                {/* <View style={{ position: 'absolute', bottom: 0, height: this.state.visibleMenu ? undefined : 0, zIndex: 6, width: '100%' }}>
                    <MenuList selected={this.props.selected} data={global.globalJson} from={this.props.from.menuId} />
                </View> */}
            </View >
        );
    }
}

const styles = StyleSheet.create({
    closePanelBtn: {
        backgroundColor: 'black',
        opacity: 0,
        position: 'absolute',
        top: 0,
        zIndex: 1,
        height: '41.5%',
        width: '100%'
    },
    menuBtnCont: {
        position: 'absolute',
        width: '85%',
        height: 70,
        bottom: 0,
        backgroundColor: 'transparent'
    },
    menuBtnInnercont: {
        position: 'absolute',
        width: '100%',
        height: 50,
        bottom: 15,
        left: 15
    },
    menuBtn: {
        width: 50,
        height: '100%'
    },
    ico: {
        height: 50,
        width: 50,
    },
    slidingUpPanelCont: {
        position: 'absolute',
        bottom: 0,
        width: '100%',
        height: '65%'
      
    },
    slidingUpPanelInnerCont: {
        position: 'relative',
        width: 50,
        height: 50,
        bottom: 15,
        left: 15
    },
});

export default HBF;
