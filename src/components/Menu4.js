import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import HTML from 'react-native-render-html';

export default class Menu4 extends Component {



    filterPages() {
        var a = this.props.pages.filter(elem => { return elem.menuId == this.props.menu4.menuId });
        return a;
    }

    render() {
        return (
            <View style={styles.mainMenu4}>
                <TouchableOpacity style={[styles.menu4Item, { borderColor: this.props.isPressed ? '#da291c'  : '#909090' }]} onPress={() => { Actions.reset('HBF', { from: this.props.menu4, filtered: this.filterPages(), selected: this.props.selected, random: Math.random() })}}>
                    <HTML containerStyle={styles.menu4TextCont} baseFontStyle={styles.menu4Text} html={this.props.menu4.title} />
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = {
    mainMenu4: {
        marginBottom: 5,
        marginLeft: 0,
        width: 250,
        height: 50
        //flex: 1
    },
    menu4Item: {
        width: 230,
        alignSelf: 'flex-end',
        height: 50,
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: '#909090'
    },
    menu4TextCont: {
        backgroundColor: '#f2f2f2',
        paddingLeft: 15,
    },
    menu4Text: {
        color: '#909090', 
        fontSize: 12 
    }
}