import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image, Dimensions, TouchableOpacity } from 'react-native';
import HTML from 'react-native-render-html';
import VideoTour from './VideoTour';
import Modall from './Modall';
import LeafletButton from './LeafletButton';
import { renderVB, renderDB, renderModalforMultipleFiles, renderModalPresentation } from '../../helpers';
import SwiperFlatList from './SwiperFlatList';
import { LeafletsModule, PresentationModule, VideoTourModule, Temp3ScrollTextPointerModule } from '../../ModuleConfig'

let LeafletsOrPres = LeafletsModule || PresentationModule;

export const { width, height } = Dimensions.get('window');

export default class TextImage extends Component {

  constructor(props) {
    super(props);
    this.height = 0;
  }

  state = {
    videoPath: [],
    documentPath: [],
    imagesPath: [],
    startSwiper: false,
    dimensions: undefined,
    dimensionsScroll: undefined,
    //heightScroll: false,
    videos: false,
    documents: false,
    arrowUp: false,
    ScrollInProgress: undefined
  }

  onLayout(event) {
    if (this.state.dimensions) return
    let { width, height } = event.nativeEvent.layout;
    this.setState({ dimensions: { width, height } });
  }

  onLayoutScroll(event) {
    if (this.state.dimensionsScroll) return
    let { width, height } = event.nativeEvent.layout;
    this.setState({ dimensionsScroll: { width, height } });
  }

  componentWillMount() {
    if (this.props.page.files) {
      let videos = this.props.page.files.filter(file => file.type == 'video')

      let documents = this.props.page.files.filter(file => file.type == 'document');

      let images = this.props.files.filter(file => {
        return file.substring(file.length - 3, file.length) == 'jpg'
          || file.substring(file.length - 3, file.length) == 'png'
          || file.substring(file.length - 4, file.length) == 'jpeg'
      })

      this.setState({ videoPath: videos, documentPath: documents, imagesPath: images });
    }

  }


  hideModal = () => {
    this.setState({ videos: false, documents: false });
  }

  showModal = (which) => {
    this.setState({ [which]: true });
  }


  renderPics(w, h) {
    return this.state.imagesPath.map((pic, i) => {
      return (
        <View key={i} style={{ backgroundColor: 'white', width: w, height: h }}>
          <Modall pic={pic}>
            <Image resizeMethod='resize' style={[styles.swiperPic, { width: w, height: h }]} source={{ uri: pic }} />
          </Modall>
        </View>
      );
    })
  }


  render() {

    // if(this.state.dimensionsScroll){
    // console.log('-----' + this.state.dimensionsScroll.height)
    // }
    return (

      <View style={styles.mainView}>
        {(!this.props.fromHome && !this.props.start && LeafletsOrPres) && <LeafletButton changed={this.props.changed} page={this.props.page} />}
        <View style={styles.body}>
          {this.props.home && VideoTourModule && <VideoTour open={() => this.setState({ videos: 2 })} />}
          <View>
            <Text style={[styles.headingText, styles.headingMain]}>{this.props.templateTitle}</Text>
            <Text style={styles.headingText}>{this.props.subtitle}</Text>
          </View>

          <View style={styles.contentContainer}>

            <View style={styles.contentText}>
              <View onLayout={(event) => this.onLayoutScroll(event)} style={styles.scrollTextCont}>
                <ScrollView
                  ref='scrollText'
                  onContentSizeChange={(width, height) => {
                    this.height = height;
                  }}

                  contentContainerStyle={styles.scrollText}
                  onScroll={(e) => {
                    let UkupanScroll = e.nativeEvent.contentSize.height;
                    let ScrollContainer = e.nativeEvent.layoutMeasurement.height;
                    let ScrollInProgress = (ScrollContainer + e.nativeEvent.contentOffset.y);
                    if (ScrollInProgress == UkupanScroll) {
                      this.setState({
                        arrowUp: true
                      })
                    } else {
                      this.setState({
                        arrowUp: false
                      })
                    }
                  }}>
                  <HTML html={this.props.text}
                    contentContainerStyle={styles.scrollText} />

                </ScrollView>

              </View>
              {this.state.dimensionsScroll && this.height - this.state.dimensionsScroll.height >= 20 && Temp3ScrollTextPointerModule && (
                this.state.arrowUp ?
                  <TouchableOpacity onPress={() => { this.refs.scrollText.scrollTo({ x: 0, y: 0, animated: true }) }} style={{ alignSelf: 'center', marginTop: 15 }}>
                    <Image style={{ width: 24, height: 24 }}
                      source={require('./ico/32/up-arrow.png')} />
                  </TouchableOpacity>
                  :
                  <TouchableOpacity onPress={() => this.refs.scrollText.scrollToEnd()} style={{ alignSelf: 'center', marginTop: 15 }}>
                    <Image style={{ width: 24, height: 24 }}
                      source={require('./ico/32/down-arrow.png')} />
                  </TouchableOpacity>
              )}

            </View>

            <View style={styles.contentPic} onLayout={(event) => this.onLayout(event)}>
              <View style={styles.contentPicImg}>

                <SwiperFlatList
                  keyExtractor={(item) => item.toString()}
                  showPagination={this.state.imagesPath.length > 1 ? true : false}
                >
                  {this.state.dimensions && this.renderPics(this.state.dimensions.width, this.state.dimensions.height)}
                </SwiperFlatList>

              </View>
              <View style={styles.ButtonContainer}>
                {renderVB(this.state.videoPath, this.showModal.bind(null, 'videos'))}
                {renderDB(this.state.documentPath, this.showModal.bind(null, 'documents'))}
              </View>

            </View>

          </View>

        </View>

        {renderModalforMultipleFiles('videos', this.state.videoPath, this.state.videos, this.hideModal)}
        {renderModalforMultipleFiles('documents', this.state.documentPath, this.state.documents, this.hideModal)}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainView: {
    position: 'relative',
    height: '100%'
  },
  body: {
    height: '100%',
    paddingLeft: 40,
  },
  headingText: {
    color: '#494949',
    fontSize: 15,
  },
  headingMain: {
    paddingTop: 40,
    paddingBottom: 10,
    fontSize: 25,
    fontWeight: 'bold'
  },
  contentContainer: {
    marginTop: 20,
    flexDirection: 'row',
    flex: 1,
    width: '100%',
    height: '100%',
    marginBottom: '7%',
    alignItems: 'center',

  },
  contentText: {
    flex: 2.5,
    backgroundColor: 'white',
    justifyContent: 'flex-start',
    height: '100%',
  },
  scrollTextCont: {
    height: '85%',
    backgroundColor: '#ebeced'
  },
  scrollText: {
    backgroundColor: '#ebeced',
    padding: 10
  },
  contentPic: {
    flex: 4.5,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 30,
  },
  contentPicImg: {
    width: '100%',
    height: '85%'
  },
  swiperPic: {
    alignSelf: 'center',
    resizeMode: 'cover'
  },
  ButtonContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
    width: '100%',
    height: '15%',
  }
});
