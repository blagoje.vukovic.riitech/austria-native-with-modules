import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';

export default class DB extends Component {
    render() {

        return (

            <View style={styles.mainCont}>
                <TouchableOpacity disabled={this.props.disabled} style={styles.ButtonContent} onPress={() => Actions.DocumentView({ docuri: this.props.documenturi })}>
                    <Image
                        style={styles.ButtonIconStyle}
                        source={require('./ico/32/file.png')}
                    />
                    <Text style={styles.ButtonTextStyle}>DOCUMENT</Text>
                </TouchableOpacity >
            </View>
        );
    }
}

const styles = StyleSheet.create({
    mainCont: {
        width: 160
    },
    ButtonContent: {
        borderColor: '#fff',
        borderWidth: 3,
        borderRadius: 4,
        width: '100%',
        backgroundColor: '#CFCFCF',
        padding: 9,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    ButtonIconStyle: {
        marginRight: 10,
        width: 18,
        height: 18
    },
    ButtonTextStyle: {
        fontSize: 14,
        color: '#494F52'
    },
});