import React, { Component } from 'react';
import { StyleSheet, TextInput, Text, View, TouchableOpacity, AsyncStorage, Keyboard, Switch, Alert, ScrollView, Image, ActivityIndicator } from 'react-native';
import RNFB from 'react-native-fetch-blob';
import Modal from 'react-native-modal';
import _ from 'lodash';
import { isNetworkConnected } from '../../helpers';

export default class LeafletTestComponent extends Component {

    state = {
        forwardModal: false,
        email: '',
        person: '',
        message: '',
        userId: '',
        errorMessage: '',
        successMessage: '',
        spinner: false,
        keyboard: false,
        checked: true
    }


    componentWillMount() {
        AsyncStorage.getItem('@userId')
            .then(res => {
                if (res == null) {
                    this.setState({ userId: '' });
                } else {
                    this.setState({ userId: res });
                }
            })
    }



    renderProducts = () => {
        return global.leaflets.fullPages.map((p, i) => {
            if (global.leaflets.pages.findIndex(a => a.pageId == p.pageId) == -1) {
                global.leaflets.pages.push({ pageId: p.pageId, comment: '', checked: true });
            } else {
                // vec u nizu, ne treba gurnuti novi
            }
            let image = p.files ? p.files.find(f => f.type == 'image') : ''
            return (
                <View key={i} style={styles.productView}>
                    <View style={styles.checkBtn}>
                        <TouchableOpacity
                            onPress={() => { this.checkItOrNot(p.pageId) }}
                            style={styles.checkBtnTouch}>
                            <Image style={styles.checkImg} source={this.isItChecked(p.pageId) ? require('./ico/32/check.png') : require('./ico/32/check-not.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.product}>
                        <View style={styles.ImgTitle}>
                            <View style={styles.image}>
                                <Image style={styles.imageWH} source={image ? { uri: 'file://' + RNFB.fs.dirs.DocumentDir + '/' + image.filename } : require('./ico/img/product.png')} />
                            </View>
                            <View style={styles.title}>
                                <Text style={styles.categoryText}>Category</Text>
                                <Text style={styles.titleText}>{p.title ? p.title : 'No title'}</Text>
                            </View>
                        </View>
                        <View style={styles.commentView}>
                            <TextInput
                                keyboardType='default'
                                placeholder="Comment"
                                placeholderTextColor='#BDBDBD'
                                style={styles.input}
                                onChangeText={(text) => {
                                    this.changeCommentforPage(p.pageId, text);
                                    this.forceUpdate();
                                }}
                                value={this.getValueForComment(p.pageId)}
                            />
                            <TouchableOpacity onPress={() => { this.deleteCommentforPage(p.pageId); this.forceUpdate() }} >
                                <Image style={styles.backImg} source={require('./ico/32/back.png')} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            );
        })

    }

    getValueForComment = (id) => {
        let index = global.leaflets.pages.findIndex(p => p.pageId == id);
        if (index == -1) {
            console.log('Nema tog page-a?');
        } else {
            return global.leaflets.pages[index].comment;
        }
    }

    changeCommentforPage = (id, text) => {
        let index = global.leaflets.pages.findIndex(p => p.pageId == id);
        if (index == -1) {
            console.log('Nema tog page-a?');
        } else {
            global.leaflets.pages[index].comment = text;
        }
    }

    deleteCommentforPage = (id) => {
        let index = global.leaflets.pages.findIndex(p => p.pageId == id);
        if (index == -1) {
            console.log('Nema tog page-aaaaaaaaa?');
        } else {
            global.leaflets.pages[index].comment = '';
        }
    }

    checkAll = () => {
        global.leaflets.pages.forEach(p => {
            p.checked = true;
            this.setState({ checked: true })
        })
        this.forceUpdate();
    }

    uncheckAll = () => {
        global.leaflets.pages.forEach(p => {
            p.checked = false;
            this.setState({ checked: false })
        });
        this.forceUpdate()
    }


    checkItOrNot = (id) => {
        let index = global.leaflets.pages.findIndex(p => p.pageId == id);

        global.leaflets.pages[index].checked = !global.leaflets.pages[index].checked;
        this.forceUpdate();

    }

    isItChecked = (id) => {
        let index = global.leaflets.pages.findIndex(p => p.pageId == id);
        return global.leaflets.pages[index].checked;
    }

    deleteAllChecked = () => {
        let arr = global.leaflets.pages;
        for (let i = arr.length - 1; i >= 0; --i) {
            if (arr[i].checked) {
                _.remove(global.leaflets.fullPages, a => a.pageId == arr[i].pageId);
                arr.splice(i, 1);
            } else {
                //arr[i].checked = true;
            }
        }
        AsyncStorage.setItem('leaflets', JSON.stringify(global.leaflets));
        this.forceUpdate();
    }

    componentDidMount() {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    }

    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    _keyboardDidShow = () => {
        this.setState({ keyboard: true })
    }

    _keyboardDidHide = () => {
        this.setState({ keyboard: false })
    }

    openSendModal = () => {
        this.setState({ forwardModal: true })
    }

    closeModal = () => {
        this.setState({ forwardModal: false })
    }

    forward = () => {
        this.openSendModal();
    }

    submitOrSpinner = () => {
        if (this.state.spinner) {
            return (
                <View style={[styles.inputCont, { height: 150 }]}>
                    <ActivityIndicator color='#BDB9B9' size='large' />
                </View>
            );
        }
        return (
            <View style={styles.inputCont}>
                <TouchableOpacity onPress={() => { this.setState({ spinner: true }); isNetworkConnected() ? this.send() : Alert.alert('No internet connection') }}><View style={styles.sendBtn}><Text style={styles.modalBtnText}>SEND</Text></View></TouchableOpacity>
                <TouchableOpacity onPress={() => this.closeModal()} ><View style={styles.cancelBtn}><Text style={styles.modalBtnText}>CANCEL</Text></View></TouchableOpacity>
            </View>
        );
    }

    send = () => {
        isNetworkConnected()
            .then(res => {
                if (res) {
                    let link = global.server + '?a=ajax&do=sendLeaflet&projectId=' + global.projectId + '&token=' + global.jsonToken + global.userToken;
                    let formData = new FormData();
                    if (this.state.userId && this.state.email != '' && this.state.person != '' && this.state.message != '') {
                        let readyForSend = {};
                        readyForSend.userId = this.state.userId;
                        readyForSend.sendToEmail = this.state.email;
                        readyForSend.sendToPerson = this.state.person;
                        readyForSend.message = this.state.message;
                        readyForSend.pages = global.leaflets.pages.filter(p => p.checked);
                        readyForSend.pages = readyForSend.pages.map(p => _.omit(p, ["checked"]));
                        Object.keys(readyForSend).forEach(key => formData.append(key, JSON.stringify(readyForSend[key])));
                        //formData.append(readyForSend)
                        console.log(formData);
                        fetch(link, { method: 'POST', body: formData })
                            .then((res) => res.json())
                            .then((res) => {
                                console.log(res)
                                this.setState({ spinner: false })
                                if (res.result == 'ok') {
                                    Alert.alert('SUCCESS!', 'Leaflet successfully sent.', [{ text: 'OK', onPress: () => this.closeModal() }])
                                    //AsyncStorage.setItem('leaflets', JSON.stringify({ userId: false, sendToEmail: false, sendToPerson: false, message: false, pages: [], fullPages: [] }));
                                    //global.leaflets = { userId: false, sendToEmail: false, sendToPerson: false, message: false, pages: [], fullPages: [] };
                                    this.forceUpdate();
                                } else {
                                    this.setState({ spinner: false })
                                    Alert.alert('Error', 'Something went wrong. Please try again later', [{ text: 'OK', onPress: () => { } }])
                                }
                            })
                            .catch(e => { { this.setState({ spinner: false }); Alert.alert('Error', 'Something went wrong. Check your internet connection and try again later.', [{ text: 'OK', onPress: () => { } }]) } })

                    } else if (this.state.userId === '') {
                        this.setState({ spinner: false })
                        Alert.alert('Error', 'You must be logged in to send a leaflet', [{ text: 'OK', onPress: () => { } }])
                    } else {
                        this.setState({ spinner: false })
                        Alert.alert('Warning', 'Please fill in all fields in order to send a leaflet', [{ text: 'OK', onPress: () => { } }])
                    }

                } else {
                    this.setState({ spinner: false })
                    Alert.alert('Error', 'Something went wrong. Check your internet connection and try again later.', [{ text: 'OK', onPress: () => { } }])
                }
            })
    }

    render() {
        return (
            <View style={styles.content}>
                <View style={styles.renderProductsCont}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        {this.renderProducts()}
                    </ScrollView>
                </View>

                <View style={styles.leafletButtonsMainCont}>
                    <View style={styles.leafletButtonsInnerCont}>

                        <TouchableOpacity onPress={() => { this.state.checked ? this.uncheckAll() : this.checkAll()}} style={styles.selectAllBtn}>
                            <View style={styles.buttonsImgCont}>
                                <Image style={styles.buttonsImg} source={require('./ico/32/tick.png')} />
                            </View>
                            <View style={styles.buttonsTextCont}>
                                {!this.state.checked ? <Text style={styles.buttonsText}>Select all</Text> : <Text style={styles.buttonsText}>Deselect all</Text>}
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.forward()} style={styles.ForDelBtn}>
                            <View style={styles.buttonsImgCont}>
                                <Image style={styles.buttonsImg} source={require('./ico/32/tick-white.png')} />
                            </View>
                            <View style={styles.buttonsTextCont}>
                                <Text style={[styles.buttonsText, { color: 'white' }]}>Forward</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.deleteAllChecked()} style={[styles.ForDelBtn, { backgroundColor: '#da281c' }]}>
                            <View style={styles.buttonsImgCont}>
                                <Image style={styles.buttonsImg} source={require('./ico/32/cancel-white.png')} />
                            </View>
                            <View style={styles.buttonsTextCont}>
                                <Text style={[styles.buttonsText, { color: 'white' }]}>Delete</Text>
                            </View>
                        </TouchableOpacity>
                    </View>

                </View>
                <Modal
                    isVisible={this.state.forwardModal}
                    onBackdropPress={() => this.closeModal()}
                    style={this.state.keyboard ? styles.modalKeyboardAware : ''}
                >
                    <View style={[styles.modalMainCont, { paddingTop: this.state.keyboard ? 0 : 50}]}>
                        {!this.state.keyboard &&
                            <Text style={styles.modalTitle}>SEND LEAFLET</Text>
                        }
                        <ScrollView style={{ width: '100%', height: '100%' }} contentContainerStyle={styles.avoidScroll} showsVerticalScrollIndicator={false}>

                            <View style={styles.inputCont}>
                                <Text style={styles.inputTitle}>E-MAIL RECIPIENT: </Text>
                                <TextInput
                                    underlineColorAndroid='transparent'
                                    keyboardType='email-address'
                                    placeholder="Type in recipient's email"
                                    placeholderTextColor='#BDBDBD'
                                    style={styles.inputModal}
                                    onChangeText={(text) => {
                                        this.setState({ email: text })
                                    }}
                                    value={this.state.email}
                                    onSubmitEditing={() => this.person.focus()}
                                    returnKeyType="next"
                                />
                            </View>

                            <View style={styles.inputCont}>
                                <Text style={styles.inputTitle}>PERSON: </Text>
                                <TextInput
                                    underlineColorAndroid='transparent'
                                    keyboardType='default'
                                    placeholder="Sender name"
                                    placeholderTextColor='#BDBDBD'
                                    style={styles.inputModal}
                                    onChangeText={(text) => {
                                        this.setState({ person: text })
                                    }}
                                    value={this.state.person}
                                    ref={(input) => this.person = input}
                                    onSubmitEditing={() => this.message.focus()}
                                    returnKeyType="next"
                                />
                            </View>

                            <View style={styles.inputCont}>
                                <Text style={styles.inputTitle}>MESSAGE: </Text>
                                <TextInput
                                    underlineColorAndroid='transparent'
                                    multiline={true}
                                    keyboardType='default'
                                    placeholder="Type in message"
                                    placeholderTextColor='#BDBDBD'
                                    style={styles.inputMessage}
                                    onChangeText={(text) => {
                                        this.setState({ message: text })
                                    }}
                                    value={this.state.message}
                                    ref={(input) => this.message = input}
                                />
                            </View>

                            {this.submitOrSpinner()}

                        </ScrollView>
                        {/* {this.state.successMessage != '' && <View style={styles.successMessage}><Text style={{ alignSelf: 'center', color: 'green' }}>{this.state.successMessage}</Text></View>}
                                      {this.state.errorMessage != '' && <View style={styles.successMessage}><Text style={{ alignSelf: 'center', color: 'red' }}>{this.state.errorMessage}</Text></View>} */}
                    </View>
                </Modal>

            </View>
        );
    }
}
const styles = StyleSheet.create({
    productView: {
        flex: 1,
        flexDirection: 'row',
        width: '100%',
        height: 200,
        marginLeft: '5%',
        flexDirection: 'row',
        paddingBottom: 20,
        paddingTop: 20,
        borderBottomWidth: 0.5,
        borderColor: 'black'
    },
    checkBtn: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        height: '100%',
        paddingTop: 50
    },
    checkBtnTouch: {
        height: '100%',
        width: '100%',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    checkImg: {
        height: 15,
        width: 15,
        marginLeft: 10
    },
    product: {
        flex: 10,
        alignItems: 'flex-start',
        justifyContent: 'center',
        width: '100%',
        height: '100%'
    },
    ImgTitle: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        height: '100%',
        flexDirection: 'row'
    },
    image: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        height: '100%'
    },
    imageWH: {
        height: 90,
        width: 120
    },
    title: {
        flex: 3,
        alignItems: 'flex-start',
        justifyContent: 'center',
        width: '100%',
        height: '100%'
    },
    categoryText: {
        color: '#da281c',
        fontSize: 14,
        padding: 5
    },
    titleText: {
        fontSize: 18,
        padding: 5,
        color: 'black'
    },
    commentView: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start',
        width: '90%',
        height: '100%',
        flexDirection: 'row'
    },
    input: {
        backgroundColor: 'white',
        width: '90%',
        height: 50,
        fontSize: 14,
        marginLeft: 10,
        textAlign: 'left'
    },
    backImg: {
        height: 15,
        width: 15
    },
    content: {
        backgroundColor: 'white',
        width: '100%',
        height: '93%',
        flexDirection: 'row',
        position: 'absolute',
        top: '7%',
        zIndex: 3,
        padding: 20,
        paddingRight: 5
    },
    renderProductsCont: {
        width: '65%',
        height: '100%'
    },
    leafletButtonsMainCont: {
        width: '35%',
        height: '100%',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    leafletButtonsInnerCont: {
        width: '70%',
        height: '30%',
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: 50
    },
    selectAllBtn: {
        flex: 1,
        width: '100%',
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 1,
        marginBottom: 10
    },
    buttonsImgCont: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonsImg: {
        height: 15,
        width: 15
    },
    buttonsTextCont: {
        flex: 4,
        alignItems: 'flex-start',
        justifyContent: 'center'
    },
    buttonsText: {
        padding: 5,
        textAlign: 'left',
        color: 'black',
        fontSize: 18
    },
    ForDelBtn: {
        flex: 1,
        width: '100%',
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 10,
        backgroundColor: '#b7bf11'
    },
    modalKeyboardAware: {
        flex: 0,
        height: '100%',
        marginTop: 0,
        justifyContent: 'flex-start'
    },
    modalMainCont: {
        backgroundColor: 'white',
        flex: 1,
        justifyContent: 'center',
        width: '40%',
        marginLeft: 'auto',
        marginRight: 'auto',
        paddingLeft: 40,
        paddingRight: 40
    },
    modalTitle: {
        fontSize: 25,
        textAlign: 'center',
        fontWeight: 'bold'
    },
    inputCont: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%'
    },
    inputModal: {
        backgroundColor: 'white',
        width: '100%',
        height: 50,
        fontSize: 14,
        textAlign: 'left',
        borderBottomWidth: 1,
        borderColor: '#CFCFCF'
    },
    avoidScroll: {
        justifyContent: 'center',
        paddingTop: '5%'
    },
    inputTitle: {
        paddingTop: 15,
        paddingBottom: 5,
        alignSelf: 'flex-start',
        fontSize: 12
    },
    inputMessage: {
        backgroundColor: 'white',
        width: '100%',
        height: 80,
        fontSize: 14,
        textAlign: 'left',
        borderBottomWidth: 1,
        borderColor: '#CFCFCF'
    },
    sendBtn: {
        padding: 10,
        backgroundColor: '#d8d8d8',
        marginTop: 20,
        width: 300
    },
    modalBtnText: {
        textAlign: 'center',
        color: '#757575'
    },
    cancelBtn: {
        padding: 10,
        borderColor: '#757575',
        borderWidth: 1,
        backgroundColor: '#fff',
        marginTop: 10,
        width: 300
    },
    successMessage: {
        justifyContent: 'center',
        width: '100%',
        padding: 10
    }
});