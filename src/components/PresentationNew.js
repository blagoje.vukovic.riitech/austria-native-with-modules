import React, { Component } from 'react';
import { StyleSheet, TextInput, Text, View, TouchableOpacity, Image, ScrollView, AsyncStorage, Alert } from 'react-native';
import Modal from "react-native-modal";
import * as Progress from 'react-native-progress';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import PresentationSort from './PresentationSort';
import { Actions } from 'react-native-router-flux';
import _ from 'lodash';


export default class PresentationNewComponent extends Component {

  state = {
    text: '',
    sort: false,
    prezentacije: [],
    error: undefined,
    itsPresentations: [],
    itsName: '',
    userId: null,
    filterPrezentacije: [],
    addAnother: false,
    editClicked: false,
    spinner: true
  }

  createNewPresentation = () => {
    // console.log('konzola od e', e)
    let foundObject = this.state.prezentacije.find(pre => (
      pre.ime == this.state.text && pre.languageId == global.languageId && pre.userId == this.state.userId
    ));
    console.log(this.state.text, 'is text and this is foundPresentationOBJ', foundObject);
    if (!foundObject) {
      let prezentacija = [{
        ime: this.state.text,
        pages: [],
        languageId: global.languageId,
        userId: this.state.userId
      }]
      novePrezentacije = prezentacija.concat(this.state.prezentacije);
      this.setState({ prezentacije: novePrezentacije, editClicked: false });
      AsyncStorage.setItem('Prezentacije', JSON.stringify(novePrezentacije))
      this.setState({ text: '' })
    } else {
      Alert.alert('Presentation already exists!', 'Please change the title of your presentation', [{ text: 'OK', onPress: () => { } }])
    }
  }

  deleteSelectedPresentation = (ime) => {
    Alert.alert('About to delete presentation ' + ime + '.', 'Are you sure?',
      [
        {
          text: 'Yes', onPress: () => {
            const deleteablePresentation = this.state.filterPrezentacije.find(fp => fp.ime == ime && fp.languageId == global.languageId);
            const novePrezentacije = this.state.prezentacije.filter(p => !_.isEqual(p, deleteablePresentation));
            AsyncStorage.setItem('Prezentacije', JSON.stringify(novePrezentacije))
              .then(() => this.setState({ prezentacije: novePrezentacije, editClicked: false }))
          }
        },
        { text: 'No', noPress: () => { } }
      ])

  }

  loadMe = () => {
    try {
      AsyncStorage.getItem('@userId')
        .then(res => JSON.parse(res))
        .then(res => { res ? this.setState({ userId: res }) : null; return Promise.resolve() })
        .then(() =>
          AsyncStorage.getItem('Prezentacije')
            .then(res => JSON.parse(res))
            .then(res => { res ? this.setState({ prezentacije: res }) : ''; return Promise.resolve() })
            .then(() => {
              this.state.prezentacije ?
                this.setState({
                  filterPrezentacije: this.state.prezentacije.filter(p =>
                    p.languageId == global.languageId && p.userId == this.state.userId),
                  spinner: false
                })
                :
                null; return Promise.resolve()
            })
        );

    } catch (e) {
      console.log('ERROR async cwm presentationNew', e)
    }
  }

  componentWillMount() {
    this.loadMe();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.prezentacije !== this.state.prezentacije) {
      this.setState({ filterPrezentacije: this.state.prezentacije.filter(p => p.languageId == global.languageId && p.userId == this.state.userId) })
    }
  }

  addPagesToPresentation = (imePrezentacije) => {
    if (!this.state.sort && !this.props.lookingAt) {
      this.setState({ sort: true });
    } else {
      if (this.state.filterPrezentacije) {
        console.log('lookingAt', this.props.lookingAt)
        let modP = this.state.filterPrezentacije.find(p => imePrezentacije == p.ime);
        if (this.props.lookingAt) {
          if (!modP.pages.find(pa => pa == this.props.lookingAt.pageId)) {
            modP.pages = modP.pages.concat(this.props.lookingAt.pageId);
            const nP = this.state.prezentacije.map(pr => {
              if (pr.ime == imePrezentacije && pr.languageId == global.languageId && pr.userId == this.state.userId) {
                return modP;
              } else {
                return pr;
              }
            })
            AsyncStorage.setItem('Prezentacije', JSON.stringify(nP));
          } else {
            console.log('nista')
          }
        } else {
          Alert.alert('This is not page!', 'Please select page for your presentation', [{ text: 'OK', onPress: () => { Actions.refresh({ visiblepresentation: true, pagePresentation: this.props.page }) } }])
        }
      }
    }

  }

  whatIcon = (p) => {
    if (!this.props.lookingAt) {
      return require('./ico/img/pres.jpg');
    } else {
      if (p.pages.findIndex(a => a == this.props.lookingAt.pageId) >= 0) {
        return require('./ico/img/checked2.png');
      } else {
        return require('./ico/img/add.png');
      }
    }
  }

  refreshItsPresentations = (fullPages, itsName) => {
    this.loadMe();
  }

  //TODO DZI maderfakeru: Klik na dodaj page u presentation, ne dodajes page, nego zatvori prozor, otvori header, opet mi otvara iz lookingat. FIX IT!

  presentationSort = () => {
    if (this.state.sort) {
      return <PresentationSort
        svePrezentacije={this.state.prezentacije}
        user={this.state.userId}
        itsPresentations={this.state.itsPresentations}
        itsName={this.state.itsName}
        clickDone={() => {
          this.setState({ sort: false, editClicked: false });
          Actions.refresh({ visiblepresentation: true })
        }
        }
        refreshItsPresentations={this.refreshItsPresentations}
        userId={this.state.userId}
      />
    } else {
      return (
        <View style={styles.mainCont}>

          <View style={styles.headerCont}>

            <View style={styles.inputView}>
              <View style={styles.innerInputCont}>
                <TextInput
                  keyboardType='default'
                  placeholder="New presentation"
                  underlineColorAndroid="transparent"
                  style={styles.input}
                  onChangeText={(text) => {
                    this.setState({ text });
                  }}
                  value={this.state.text}
                />
                <TouchableOpacity style={{ alignSelf: 'center' }} onPress={() => { this.createNewPresentation() }} >
                  <Image style={styles.addNewPresIcon} source={require('./ico/32/addP.png')} />
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.backBtnCont}>
              {(!this.props.lookingAt && this.state.filterPrezentacije.length > 0) &&
                <TouchableOpacity style={styles.backBtn} onPress={() => this.setState({ editClicked: !this.state.editClicked })}>
                  {this.state.editClicked ? <Text style={styles.doneText}>Done</Text> : <View style={styles.editTextCont}><Text style={styles.editText}>Edit</Text><Image style={styles.editIcon} source={require('./ico/img/edit.png')} /></View>}
                </TouchableOpacity>
              }
              {this.props.lookingAt &&
                <TouchableOpacity style={styles.closePresentationBtn} onPress={() => Actions.refresh({ visiblepresentation: false, pagePresentation: this.props.page })}>
                  <Image style={styles.wh100} source={require('./ico/32/back.png')} />
                </TouchableOpacity>
              }
            </View>
          </View>
          {
            this.props.lookingAt ?
              <View>
                {this.state.filterPrezentacije.length > 0 && <Text style={styles.addTo}>Add to ...</Text>}
              </View>
              :
              <Text style={styles.textPresentation}>Presentations:</Text>

          }

          <View style={styles.presentationView}>
            <ScrollView contentContainerStyle={styles.innerPresentationScroll} showsVerticalScrollIndicator={false}>
              {this.state.filterPrezentacije.map(p => {
                return (
                  <View style={styles.touchableCont} key={p.ime}>
                    <View style={styles.innerTouchableCont}>
                      <TouchableOpacity
                        style={[styles.presentation, { borderWidth: this.props.lookingAt ? 2 : 0, borderColor: this.props.lookingAt ? '#3d3d3d' : 'transparent', borderRadius: this.props.lookingAt ? 3 : 0 }]}
                        onPress={() => {
                          // this.setState({ sort: true, itsPresentations: p.pages, itsName: p.ime });
                          this.props.lookingAt ?
                            this.addPagesToPresentation(p.ime)
                            :
                            this.setState({ sort: true, itsPresentations: p.pages, itsName: p.ime });
                          this.forceUpdate();
                        }}>
                        {this.state.editClicked === true ?
                          <View style={styles.presentationImageCont}>
                            <Image style={styles.presentationImg} source={this.whatIcon(p)} />
                            <TouchableOpacity activeOpacity={1} style={styles.deletePresButton} onPress={() => { this.deleteSelectedPresentation(p.ime) }} >
                              <Image style={styles.deleteImg} source={require('./ico/img/delete.png')} />
                            </TouchableOpacity>
                          </View>
                          :
                          <View style={styles.presentationImageCont}>
                            <Image style={styles.presentationImg} source={this.whatIcon(p)} />
                          </View>
                        }
                      </TouchableOpacity>

                      <View style={styles.presentationTitleCont}>
                        <Text style={styles.presentationTitle}>{p.ime}</Text>
                      </View>
                    </View>
                  </View>
                )
              })}
            </ScrollView>
          </View>
        </View>
      );
    }
  }

  render() {
    if (this.state.spinner) {
      return null;
    }
    else {
      if (this.state.userId) {
        return (
          <View style={styles.content}>
            {this.presentationSort()}
          </View>
        )
      }
      else {
        return (
          <View style={[styles.content, { justifyContent: 'center' }]}>
            {/*ispisi listu prezentacija*/}
            <View style={{ alignSelf: 'center' }}>
              <Text style={styles.notLoggedin}>In order to use this feature you need to be logged in. Please go to Settings to log in.</Text>
            </View>
          </View>

        )
      }
    }
  }
}
const styles = StyleSheet.create({
  mainCont: {
    width: '100%',
    height: '100%',
    position: 'relative'
  },
  headerCont: {
    width: '100%',
    flexDirection: 'row',
    marginTop: 30
  },
  inputView: {
    flex: 3,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  innerInputCont: {
    flexDirection: 'row'
  },
  input: {
    backgroundColor: 'white',
    width: 300,
    height: 50,
    fontSize: 24,
    borderBottomWidth: 1,
    borderBottomColor: '#757575'
  },
  addNewPresIcon: {
    height: 20,
    width: 20
  },
  backBtnCont: {
    justifyContent: 'center',
    alignItems: 'flex-end',
    width: '50%',
    paddingRight: '5%'
  },
  backBtn: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  doneText: {
    fontSize: 20,
  },
  editTextCont: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  editText: {
    fontSize: 20,
    paddingRight: 5,
  },
  editIcon: {
    height: 20,
    width: 20
  },
  closePresentationBtn: {
    width: 25,
    height: 25,
    marginRight: 40
  },
  wh100: {
    height: '100%',
    width: '100%',
  },
  addTo: {
    paddingLeft: '4%',
    margin: 15,
    fontSize: 18
  },
  textPresentation: {
    fontSize: 20,
    marginTop: 25,
    marginBottom: 10
  },
  presentationView: {
    flex: 20,
    paddingLeft: '4%',
    width: '80%',
    marginTop: 10
  },
  innerPresentationScroll: {
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  touchableCont: {
    width: '50%',
    height: 100,
    marginBottom: 20,
    flexDirection: 'row',
    position: 'relative',
  },
  innerTouchableCont: {
    width: '100%',
    height: '100%',
    flexDirection: 'row',
    position: 'relative'
  },
  presentation: {
    height: '100%',
    width: '40%',
    position: 'relative'
  },
  presentationImageCont: {
    height: '100%',
    width: '100%',
    position: 'relative'
  },
  presentationImg: {
    padding: 5,
    height: '100%',
    width: '100%',
    position: 'relative'
  },
  deletePresButton: {
    position: 'absolute',
    width: '100%',
    height: '100%'
  },
  deleteImg: {
    width: 16,
    height: 16,
    alignSelf: 'flex-end',
    top: 2,
    right: 2
  },
  presentationTitleCont: {
    height: '100%',
    width: '70%',
    justifyContent: 'center'
  },
  presentationTitle: {
    paddingLeft: 15,
    fontSize: 18
  },
  content: {
    backgroundColor: 'white',
    width: '100%',
    height: '93%',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    position: 'absolute',
    top: '7%',
    flex: 1,
    zIndex: 3,
    paddingLeft: '5%'
  },
  notLoggedin: {
    fontSize: 24,
    textAlign: 'center'
  }
});


