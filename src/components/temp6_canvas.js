import React, { Component } from 'react';
import { View, WebView, StyleSheet, Alert } from 'react-native';
import RNFB from 'rn-fetch-blob';
import { findPageObjectById, findMenuObjectById, findMenu1Selected } from '../../helpers/index';
import { Actions } from 'react-native-router-flux';


export default class Canvas extends Component {

    findVideo = (idFajla) => {
        console.log('====== usao u funkciju', idFajla)
        let a = global.globalJson.files.find(file => file.pageId == idFajla)
        console.log('=====', a)
        return a
    }

    onMessage = (event) => {
        //Prints out data that was passed.
        let data = JSON.parse(event.nativeEvent.data)
        // console.log('========')
        // console.log('onMessage: ', data);
        // console.log('========')
        let a = global.globalJson.pages.find(b => b.pageId == data.pageid)
        let filtered = findPageObjectById(a.menuId);
        let indexStart = filtered.findIndex(page => page.pageId == data.pageid)
        let from = findMenuObjectById(filtered[indexStart].menuId);
        let selected = findMenu1Selected(from);
        if (data.type == 'page') {
            Actions.reset('HBF', { filtered, from, selected, indexStart });
        } else {
            data.type == 'VideoView' ? Actions.VideoView({ videouri: 'file://' + RNFB.fs.dirs.DocumentDir + '/' + this.findVideo(data.pageid).filename }) : Actions.DocumentView({ docuri: 'file://' + RNFB.fs.dirs.DocumentDir + '/' + this.findVideo(data.pageid).filename })
        }
    }
    componentWillMount() {
        this.props.temp6prop(true)
    }

    //   findInfoAboutMenu = (p) => {
    //     // this.props.from == ceo objekat menija
    //     // this.props.selected == index od meni 1
    //     // this.props.filtered == [ pageObj ](
    //     let a = global.globalJson.pages.find(b => b.pageId == p)
    //     let filtered = findPageObjectById(a.menuId);
    //     let indexStart = filtered.findIndex(page => page.pageId == p);
    //     console.log(indexStart);
    //     let from = findMenuObjectById(filtered[indexStart].menuId);
    //     let selected = findMenu1Selected(from);
    //     return { filtered, from, selected };
    // }
    // Actions.VideoView({ videouri: 'file://' + RNFB.fs.dirs.DocumentDir + '/' + this.findVideo(spot.linkVideoId).filename, file: this.findVideo(spot.linkVideoId), repeat: false})
    render() {
       
        return (
            <View style={styles.mainView}>
                <WebView
                    style={{ width: '100%', height:'100%', overflow: 'hidden', backgroundColor: 'transparent' }}
                    source={{ uri: 'file://' + RNFB.fs.dirs.DocumentDir + '/web/' + this.props.page.webStartpage }}
                    // onMessage={this.handleMessage}
                
                    mixedContentMode="always"
                    scalesPageToFit={false}
                    javaScriptEnabled

                    domStorageEnabledd
                    thirdPartyCookiesEnabled
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainView: {
        backgroundColor: 'white',
        position: 'relative',
        flex: 1
    }
});
