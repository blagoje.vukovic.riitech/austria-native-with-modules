import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Menu4 from './Menu4';
import HTML from 'react-native-render-html';


class Menu3 extends Component {

    filterPages() {

        var a = this.props.pages.filter(elem => { return elem.menuId == this.props.menu3.menuId });
        return a;
    }

    renderMenus4 = () => {
        if (this.props.menu3.children) {
            return this.props.menu3.children.map(child =>
                <Menu4
                    key={child.menuId}
                    menu4={child}
                    pages={this.props.pages}
                    isPressed={this.props.from == child.menuId ? true : false}
                    selected={this.props.selected}
                />
            )
        }
    }

    render() {
        return (
            <View style={styles.mainMenu3}>
                <TouchableOpacity style={[styles.menu3Item, {borderColor: this.props.isPressed ? '#da291c' : '#909090' }]} onPress={() => Actions.reset('HBF', { from: this.props.menu3, filtered: this.filterPages(), selected: this.props.selected,  random: Math.random()})}>
                    <HTML containerStyle={styles.menu3TextCont} baseFontStyle={styles.menu3Text} html={this.props.menu3.title} />
                </TouchableOpacity>

                    {this.renderMenus4()}
                     
            </View>
        );
    }
}

const styles = {
    mainMenu3: {
        flexWrap: 'wrap',
        // flex: 1,
        //height: '100%'
        marginRight: 20
    },
    menu3Item: {
        marginBottom: 5,
        width: 250,
        height: 50,
        justifyContent: 'center',
        backgroundColor: '#f2f2f2',
        borderWidth: 1
    },
    menu3TextCont: {
        paddingLeft: 15        
    },
    menu3Text: {
        color: '#da291c', 
        fontSize: 12
    }
}

export default Menu3;
