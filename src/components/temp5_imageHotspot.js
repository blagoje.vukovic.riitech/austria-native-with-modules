import React, { Component } from 'react';
import { StyleSheet, View, Image, TouchableOpacity, Text, TouchableWithoutFeedback, Dimensions, ImageBackground } from 'react-native';
import RNFB from 'react-native-fetch-blob';
import { Actions } from 'react-native-router-flux';
import LeafletButton from './LeafletButton';
import VideoTour from './VideoTour';
import { findPageObjectById, findMenuObjectById, findMenu1Selected, aaa, renderVB, renderDB, renderModalforMultipleFiles, renderModalPresentation } from '../../helpers';
import { LeafletsModule, PresentationModule, VideoTourModule } from '../../ModuleConfig'

let LeafletsOrPres = LeafletsModule || PresentationModule;

const margine = 0.0;

export default class HotspotImage extends Component {

    state = {
        layoutWidth: 0,
        layoutHeigth: 0,
        picExists: false,
        videoPath: [],
        documentPath: [],
        videos: false,
        documents: false
    }


    findInfoAboutMenu = (p) => {
        // this.props.from == ceo objekat menija
        // this.props.selected == index od meni 1
        // this.props.filtered == [ pageObj ](
        let a = global.globalJson.pages.find(b => b.pageId == p)
        let filtered = findPageObjectById(a.menuId);
        console.log(filtered)
        console.log('p', p)
        let indexStart = filtered.findIndex(page => page.pageId == p);
        console.log(indexStart);
        if (indexStart != -1) {
            let from = findMenuObjectById(filtered[indexStart].menuId);
            let selected = findMenu1Selected(from);
            return { filtered, from, selected };
        } else {
            let a = global.globalJson.pages.find(b => b.pageId == this.props.page.pageId)
            let filtered = findPageObjectById(a.menuId);
            console.log(filtered)
            console.log('p', p)
            let indexStart = filtered.findIndex(page => page.pageId == this.props.page.pageId);
            let from = findMenuObjectById(filtered[indexStart].menuId);
            let selected = findMenu1Selected(from);
            return { filtered, from, selected };
        }

        // return { filtered, from, selected, indexStart } = this.props.page
    }

    hotspotRedirect = (spot) => {
        let { filtered, from, selected, indexStart } = this.findInfoAboutMenu(spot.linkPageId);
        Actions.reset('HBF', { filtered, from, selected, indexStart });
    }

    getPosistionsFromJSON = () => {
        const hotspots = this.props.page.hotspots;
        if (hotspots) {
            return (hotspots.map((spot, i) => {
                // console.log(spot.label.length, spot.label)
                let numUpper = spot.label.length - spot.label.replace(/[A-Z]/g, '').length;
                let numSpecial = spot.label.length - spot.label.replace(/[@#$%^&*()_+\[\]{};':"\\|,.<>\/?]/g, '').length
                let duzina = spot.label.length;
                const posx = this.state.layoutWidth * (spot.x / 1000) + Dimensions.get('screen').width * margine / 2 
                const posy = this.state.layoutHeigth * (spot.y / 1000) - 19;
                const zDesna = require('./ico/32/123.png');
                const sLeva = require('./ico/32/123-2.png')
                return (
                    <View key={i + '.viewMaster'} style={[styles.hotspotCont, { left: spot.swapSide == 0 ? this.offset(posx) : this.offset(posx) - duzina * 9, top: posy - 15}]}>
                        <TouchableOpacity
                            activeOpacity={0.8}
                            key={i}
                            style={{ paddingTop: 17}}
                            onPress={() => {
                                let { filtered, from, selected, indexStart } = spot.linkPageId != '0' ? this.findInfoAboutMenu(spot.linkPageId) : this.findInfoAboutMenu(this.props.page.pageId)
                                Actions.reset('HBF', { filtered, from, selected, indexStart });
                                console.log(spot.label.length + ' 0000000000000054545454')
                            }}>
                            <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: spot.swapSide == 0 ? 'row' : 'row-reverse' }}>
                                {spot.highlight == 0 ? <Image key={i + '.image'} style={{ width: 24, height: 24 }} source={require('./ico/32/hotspot.png')} /> : <Image key={i + '.image'} style={{ width: 24, height: 24 }} source={require('./ico/32/hotspot-highlight.png')} />}
                                <View style={[styles.spotContainer, {right: duzina > 40 == 0 ? 10 : 35}]}>
                                    <ImageBackground key={i + '.viewSlave'}
                                        style={[styles.spotRectImg, 
                                            spot.label.length <= 11 ? { width: 120, left: spot.swapSide == 0 ? 10 : 0, right: spot.swapSide == 0 ? 0 : 10 } : 
                                            spot.label.length <= 26 ? { width: duzina * 9 + numUpper + numSpecial * 10, left: spot.swapSide == 0 ? 3 : 0, right: spot.swapSide == 0 ? 0 : 3 } : 
                                            spot.label.length <= 32 ? { width: 260, right: spot.swapSide == 0 ? 15 : 0, left: spot.swapSide == 0 ? 0 : 10 } : 
                                            spot.label.length <= 40 ? { width: 300, right: spot.swapSide == 0 ? 20 : 0, left: spot.swapSide == 0 ? 0 : 20 } : 
                                            { width: 400, right: spot.swapSide == 0 ? 25 : 0, left: spot.swapSide == 0 ? 0 : 25}]}
                                        source={spot.swapSide == 0 ? zDesna : sLeva}
                                    >
                                        <View style={[styles.hotspotTextCont, { alignSelf: 'flex-start' }, 
                                        spot.label.length <= 25 ? { marginLeft: spot.swapSide == 0 ? 30 : 0, marginRight: spot.swapSide == 0 ? 0 : 30 } : 
                                        spot.label.length <= 25 ? { marginLeft: spot.swapSide == 0 ? 30 : 0, marginRight: spot.swapSide == 0 ? 0 : 30 } : 
                                        spot.label.length <= 32 ? { marginLeft: spot.swapSide == 0 ? 35 : 0, marginRight: spot.swapSide == 0 ? 0 : 35 } : 
                                        spot.label.length <= 40 ? { marginLeft: spot.swapSide == 0 ? 45 : 0, marginRight: spot.swapSide == 0 ? 0 : 45 } : 
                                        {marginLeft: spot.swapSide == 0 ? 55 : 0, marginRight: spot.swapSide == 0 ? 0 : 55}]}>
                                            <Text numberOfLines={1} key={i + '.text'} style={[styles.hotspotTitle, spot.highlight == 1 ? { fontWeight: 'bold' } : '']}>{spot.label}</Text>
                                        </View>
                                    </ImageBackground>
                                </View>
                            </View>

                        </TouchableOpacity>
                    </View>
                );
            }));
        }
    }

    offset = (posx) => {
        if (posx < this.state.layoutWidth / 2) {
            return posx - (this.state.layoutWidth / 2 - posx) / 18;
        } else {
            return posx - (this.state.layoutWidth / 2 - posx) / 40;
        }
    };

    hideModal = () => {
        this.setState({ videos: false, documents: false });
    }

    showModal = (which) => {
        this.setState({ [which]: true });
    }

    componentWillMount() {
        RNFB.fs.exists(RNFB.fs.dirs.DocumentDir + '/' + this.props.page.files.find(e => e.type == 'image').filename)
            .then(res => res ? this.setState({ picExists: true }) : this.setState({ picExists: false }));
        if (this.props.page.files) {
            let videos = this.props.page.files.filter(file => file.type == 'video')

            let documents = this.props.page.files.filter(file => file.type == 'document');

            this.setState({ videoPath: videos, documentPath: documents });
        }
    }

    render() {
        console.log(this.props.page.hotspots)
        return (
            <View style={styles.mainView} >
                {(!this.props.fromHome && !this.props.start && LeafletsOrPres) && <LeafletButton changed={this.props.changed} page={this.props.page} />}
                <View style={styles.body}>
                {this.props.home && VideoTourModule && <VideoTour open={()=> this.setState({videos: 2})}/>}
                    <View style={styles.contentContainer}>
                        {<Image
                            ref='_image'
                            resizeMode='cover'
                            style={styles.imageStyle}
                            onLayout={event => {
                                const width = event.nativeEvent.layout.width;
                                const height = event.nativeEvent.layout.height;
                                this.setState(() => ({ layoutWidth: width, layoutHeigth: height }));
                            }}
                            source={{ uri: 'file://' + RNFB.fs.dirs.DocumentDir + '/' + this.props.page.files.find(e => e.type == 'image').filename }} />}
                        {this.state.picExists && this.getPosistionsFromJSON()}
                    </View>
                    <View style={styles.ButtonContainer}>
                        {renderVB(this.state.videoPath, this.showModal.bind(null, 'videos'))}
                        {renderDB(this.state.documentPath, this.showModal.bind(null, 'documents'))}
                    </View>
                </View>
                {renderModalforMultipleFiles('videos', this.state.videoPath, this.state.videos, this.hideModal)}
                {renderModalforMultipleFiles('documents', this.state.documentPath, this.state.documents, this.hideModal)}
            </View>
        );
    }
}


const styles = StyleSheet.create({
    mainView: {
        backgroundColor: 'white',
        position: 'relative',
        height: '100%'
    },
    body: {
        height: '100%',
        width: '100%'
    },
    contentContainer: {
        // top: -Dimensions.get('screen').scale / (10 + Math.ceil((Dimensions.get('screen').width / Dimensions.get('screen').height))*2)  * Dimensions.get('screen').height,
        marginTop: 0,
        marginBottom: 5,
        // top: '-17%',
        // top: -((Dimensions.get('screen').width / Dimensions.get('screen').height)*top) + '%',
        /*top: -((Dimensions.get('screen').width / Dimensions.get('screen').height) *
            Dimensions.get('screen').scale / ((Dimensions.get('screen').width / Dimensions.get('screen').height)) <= 1 ?
            (Dimensions.get('screen').scale / (Dimensions.get('screen').width / Dimensions.get('screen').height) * 10) + '%' :
            (Dimensions.get('screen').width / Dimensions.get('screen').height) / Dimensions.get('screen').scale * 10  ) + '%',*/
    },
    imageStyle: {
        width: Dimensions.get('screen').width - Dimensions.get('screen').width * margine,
        height: Dimensions.get('screen').height - Dimensions.get('screen').height * margine,
        resizeMode: 'cover',
        zIndex: 1,
        marginLeft: Dimensions.get('screen').width * margine / 2
    },
    hotspotCont: {
        position: "absolute",
        zIndex: 26,
        //width: 40,
        height: 50
    },
    spotContainer: {
        bottom: 7
    },
    spotRectImg: {
        borderTopLeftRadius: 25,
        backgroundColor: 'transparent',
        height: 30,
        //width: 260,
        alignItems: 'flex-start',
        justifyContent: 'center',
        borderWidth: 2,
        borderColor: 'red'
    },
    hotspotTextCont: {
        height: 33,
        // width: 260,
        justifyContent: 'center',
        alignItems: 'center'
    },
    hotspotTitle: {
        fontSize: 14,
        color: '#da281c',
        paddingBottom: 3,
        marginHorizontal: 3,
        width: '100%'
    },
    ButtonContainer: {
        position: 'absolute',
        zIndex: 25,
        width: '30%',
        height: '10%',
        alignSelf: 'flex-end',
        justifyContent: 'flex-end',
        alignItems: 'center',
        flexDirection: 'row',
        bottom: 25,
        right: 25
    }
});
