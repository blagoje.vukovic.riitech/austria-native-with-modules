import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, Image } from 'react-native';
import Modal from 'react-native-modal';

class DownloadModal extends Component {

    // this.props.isVisible
    // this.props.buttons
    // this.props.modalMessage
    // this.props.modalMessage2
    // this.props.close
    state = {
        niz: []
    }

    checkOrNot = (type) => {
        global.dlTypes[type].checked = !global.dlTypes[type].checked
        this.forceUpdate()
    }

    selectedArray = (what) => {
        return global.dlTypes[what].checked
    }
    size = (arr) => {
        let result = 0;
        if (arr) {
            global.dlTypes[arr].files.map(ele => {
                result += Number(ele.size)
            })
            result = (result / 1024 / 1024).toFixed(2)
            return result
        }
    }

    totalSize = () => {
        let result = 0;
        if (global.dlTypes) {
            Object.keys(global.dlTypes).map(e => {
                if (global.dlTypes[e].checked) {
                    global.dlTypes[e].files.map(f => {
                        result += Number(f.size)
                    })
                }
            })
            result = (result / 1024 / 1024).toFixed(2)
            // this.props.getResult(result)
            return result;
        }
    }

    completeSelselectedArray = () => {
        let array = [];
        Object.keys(global.dlTypes).map(e => {
            if (global.dlTypes[e].checked) {
                global.dlTypes[e].files.map(f => {
                    array.push(f)
                })
            }
        })

        return array;
    }

    estimatedTime = () => {
        let est = 0;
        if(this.props.modalMessage3){
            return est = this.props.modalMessage3 != 0 ? (this.totalSize() / this.props.modalMessage3 / 60).toFixed(0) + ' minutes ' + ((this.totalSize() / this.props.modalMessage3).toFixed(0) % 60) + ' seconds' : 'inf.';
        }
    }

    totalNotChecked = () => {
        let failedDownloads = [];
        Object.keys(global.dlTypes).map(e => {
            if (!global.dlTypes[e].checked) {
                global.dlTypes[e].files.map(f => {
                    failedDownloads.push(f)
                })
            }
        })
        return failedDownloads;

    }

    render() {
        // console.log('=====', this.props.modalMessage3)
        return (
            <Modal
                isVisible={this.props.isVisible}
                style={styles.alertStyle}
                onBackdropPress={() => this.props.close ? () => {} : () => { }}
            >
                <View style={{ flex: 1 }}>
                    <View style={styles.modalContainer}>

                        <View style={{ flex: 2.5, alignItems: 'center', justifyContent: 'space-between', marginVertical: 25 }}>
                            <View style={{ flex: 1, justifyContent: 'center' }}><Text style={styles.tekst1}>{this.props.modalMessage}</Text></View>
                            {this.props.modalMessage2 &&
                                <View style={{ flex: 2, width: 300, height: '100%', flexDirection: 'column', justifyContent: 'space-around' }}>
                                    {Object.keys(this.props.modalMessage2).map((e, i) => (
                                        // console.log(this.completeSelselectedArray()),
                                        <TouchableOpacity disabled={this.size(e) != 0 ? false : true} style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between' }} key={i} onPress={() => this.checkOrNot(e)} >
                                            <View style={{ flexDirection: 'row' }}>
                                                <Image style={{ height: 25, width: 25, marginRight: 10, backgroundColor: 'white' }} source={this.selectedArray(e) ? require('./ico/32/check.png') : require('./ico/32/check-not.png')} />
                                                <Text style={{ color: '#9E9E9E', fontSize: 24 }} >{e.capitalize() + 's'}</Text>
                                            </View>
                                            <View style={{ justifyContent: 'flex-end' }}>
                                                <Text style={{ color: '#9E9E9E', fontSize: 16 }}> {this.size(e)}(MB)</Text>
                                            </View>
                                        </TouchableOpacity>
                                    ))}
                                    <Text style={{ color: '#9E9E9E', fontSize: 24, marginTop: 10 }}>{this.estimatedTime()}</Text>
                                </View>
                            }
                        </View>

                        <View style={{ width: '50%', borderWidth: 1, borderColor: 'gray'}}>
                        </View>

                        <View style={styles.renderConfirmButtons}>
                            {this.props.buttons.map(b =>
                                <TouchableOpacity disabled={this.totalSize() == 0 && b.naziv == 'Download' ? true : false} style={{ flex: 1 }} key={b.naziv} onPress={() => b.akcija(this.totalSize(), this.completeSelselectedArray(), this.totalNotChecked())}>
                                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={[styles.tekst1, { width: '50%' }]}>{b.naziv} {this.props.modalMessage2 && b.naziv == 'Download' && this.totalSize() + ' MB'}</Text>
                                    </View>
                                </TouchableOpacity>
                            )}
                        </View>
                    </View>
                </View>
            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    alertStyle: {
        width: Dimensions.get('window').width * 0.5,
        maxHeight: Dimensions.get('window').height * 0.5,
        backgroundColor: '#fff',
        marginRight: 'auto',
        marginLeft: 'auto',
        marginTop: 'auto',
        marginBottom: 'auto'
    },
    modalContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    alertButton: {
        marginTop: 10,
    },
    closeImg: {
        alignSelf: 'flex-end',
        margin: 10,
        width: Dimensions.get('window').width * 0.02,
        maxHeight: Dimensions.get('window').height * 0.02,
    },
    tekst2: {
        fontSize: Dimensions.get('window').height * 0.025,
        color: "#9E9E9E",
        textAlign: 'center',
        margin: 10
    },
    tekst1: {
        fontSize: Dimensions.get('window').height * 0.03,
        color: "#9E9E9E",
        textAlign: 'center',
    },
    whiteLine: {
        borderBottomWidth: 1,
        borderColor: 'white',
        width: '30%',
    },
    renderConfirmButtons:
        { flex: 0.7, marginTop: 10, flexDirection: 'row', justifyContent: 'space-between', width: '100%', marginTop: 20 }
});




export default DownloadModal;
