import React, { Component } from 'react';
import { StyleSheet, TextInput, Text, View, TouchableOpacity, Image, ScrollView, AsyncStorage, Alert, Dimensions, Animated } from 'react-native';
import Modal from "react-native-modal";
import * as Progress from 'react-native-progress';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import RNFB from 'react-native-fetch-blob';
import { Actions } from 'react-native-router-flux';
import { findMenuObjectById, findMenu1Selected, findMenu } from '../../helpers';
import Sortable from 'react-native-sortable-listview';
// import LinearGradient from 'react-native-linear-gradient';
import SortableGrid from 'react-native-sortable-grid'
import Popover, { PopoverTouchable } from 'react-native-modal-popover';
import _ from 'lodash';

export default class PresentationNewComponent extends Component {

  state = {
    fullPages: [],
    editToggle: false,
    isVisible: false,
    modalMessage: '',
    modalMessage2: '',
    buttons: [],
    checked: [],
    sortableNotChecked: true,
    animation: new Animated.Value(0),
  }

  componentWillMount() {
    const fullPages = global.globalJson.pages.filter(gp => {
      if (this.props.itsPresentations.includes(gp.pageId)) {
        return gp;
      }
    })
    const fullPages2 = this.props.itsPresentations.map(fps => {
      return global.globalJson.pages.find(p => p.pageId == fps)
    })
    this.setState({ fullPages: fullPages2 });
  }

  animacija = () => {
    return {
      transform: [
        {
          scaleX: this.state.animation.interpolate({
            inputRange: [0, 100],
            outputRange: [1.2, -1.5],
          })
        },
        {
          scaleY: this.state.animation.interpolate({
            inputRange: [0, 100],
            outputRange: [1.2, 1.5],
          })
        },
        {
          rotate: this.state.animation.interpolate({
            inputRange: [0, 100],
            outputRange: ['0 deg', '450 deg']
          })
        }
      ]
    }
  }

  startDelete = () => {
    this.setState({
      fullPages: this.state.fullPages.filter(p => this.state.checked.indexOf(p.pageId) == -1),
      checked: [],
      sortableNotChecked: true
    })
  }

  savePresentation = () => {
    try {
      AsyncStorage.getItem('Prezentacije')
        .then(res => JSON.parse(res))
        .then(svePrezentacije => {
          console.log('Ovo su sve prezentacije: ', svePrezentacije)
          let novPres = svePrezentacije.map(p => {
            if (this.props.itsName == p.ime && this.props.user == p.userId && p.languageId == global.languageId) {
              console.log('nasao sam tu modifikovanu za koju je kliknut done');
              console.log(p.pages)
              const nP = { ime: this.props.itsName, pages: this.state.fullPages.map(pages => pages.pageId), languageId: global.languageId, userId: this.props.user }
              console.log('Ovo je np: ', nP)
              return nP;
            } else {
              return p;
            }
          });
          console.log('Ovo su sve NOVE: ', novPres)
          AsyncStorage.setItem('Prezentacije', JSON.stringify(novPres))
            .then(() => console.log('sacuvao sam one nove...'))
        })
    } catch (e) {
      console.log(e)
    }

  }

  saveToOtherPresentation = (fullPages, whereToSaveName) => {
    const newAllPresentations = this.props.svePrezentacije.map(p => {
      if (p.ime == whereToSaveName && p.languageId == global.languageId && p.userId == this.props.userId) {
        this.props.refreshItsPresentations();
        return { ...p, pages: _.union(p.pages, fullPages.map(p => p.pageId).filter(fps => this.state.checked.includes(fps))) };
      } else {
        return p;
      }
    });
    AsyncStorage.setItem('Prezentacije', JSON.stringify(newAllPresentations));
  }

  whatIcon = (p) => {
    const theirUnion = _.union(p.pages, this.state.checked);

    if (_.isEqual(p.pages.sort(), theirUnion.sort())) {
      return require('./ico/img/checked2.png');
    } else {
      return require('./ico/img/add.png');
    }

  }

  renderOneSlide = (row) => {
    let isChecked = this.state.checked.indexOf(row.pageId) >= 0;
    if (row.files) {
      if (row.files.find(f => f.type == 'image') && isChecked) {
        return (
          <View style={styles.PresentationCheckedCont}>
            <Image resizeMethod='resize' style={[styles.presentationImg, { opacity: 0.1 }]}
              source={{ uri: 'file://' + RNFB.fs.dirs.DocumentDir + '/' + row.files.find(f => f.type == 'image').filename }} />

            <Image style={styles.tickImage} source={require('./ico/img/checked.png')} />

          </View>
        );
      } else if (row.files.find(f => f.type == 'image')) {
        return (
          <View style={styles.wh100}>
            <Image resizeMethod='resize' style={styles.presentationImg}
              source={{ uri: 'file://' + RNFB.fs.dirs.DocumentDir + '/' + row.files.find(f => f.type == 'image').filename }} />
          </View>
        )
      } else {
        if (isChecked) {
          return (
            <View style={styles.PresentationCheckedCont}>
              <Image resizeMethod='resize' style={[styles.presentationImg, { opacity: 0.1 }]}
                source={require('./ico/img/pres.jpg')} />

              <Image style={styles.tickImage} source={require('./ico/img/checked.png')} />

            </View>
          );
        } else {
          return (
            <Image style={styles.presentationImg} source={require('./ico/img/pres.jpg')} />
          );
        }
      }

    } else {
      if (isChecked) {
        return (
          <View style={styles.PresentationCheckedCont}>
            <Image resizeMethod='resize' style={[styles.presentationImg, { opacity: 0.1 }]}
              source={require('./ico/img/pres.jpg')} />

            <Image style={styles.tickImage} source={require('./ico/img/checked.png')} />

          </View>
        );
      } else {
        return (
          <Image style={styles.presentationImg} source={require('./ico/img/pres.jpg')} />
        );
      }
    }
  }

  goBack = () => (
    <TouchableOpacity
      style={styles.goBackBtn}
      onPress={() => {
        this.props.clickDone();
        this.props.refreshItsPresentations();
      }}>
      <Image style={styles.goBackImg} source={require('./ico/img/presentationLeft.png')} />
      <Text style={styles.goBackText}>Go back</Text>
    </TouchableOpacity>
  );

  magicTap = (a) => {
    if (this.state.checked.indexOf(a) >= 0) {
      console.log(a + ' nije cekiran');
      this.setState({ checked: this.state.checked.filter(c => c != a) });
      if (this.state.checked.length == 0) {
        this.setState({ sortableNotChecked: true })
      }
    } else {
      console.log(a + ' je cekiran');
      this.setState({ checked: [...this.state.checked, a], sortableNotChecked: false });
    }
    //this.forceUpdate();
  }

  render() {
    console.log('sort pages', this.state.fullPages.map(p => p.pageId))
    if (!this.state.editToggle) {
      return (
        <View style={styles.content}>
          <View style={styles.navBtnCont}>
            {this.state.fullPages.length != 0 ?
              <View style={styles.PresNavBtnCont}>
                {this.goBack()}
                <Text style={styles.titleText}>{this.props.itsName ? this.props.itsName : ''}</Text>
                <TouchableOpacity style={styles.goToEditBtn} onPress={() => { this.setState({ editToggle: !this.state.editToggle }) }}>
                  <Image style={styles.goToEditImg} source={require('./ico/32/tick.png')} />
                  <Text style={styles.goToEditText}>Go to edit mode</Text>
                </TouchableOpacity>

              </View>
              :
              this.goBack()
            }

          </View>

          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.presentationView}>

              {/*ispisi listu slajdova u prezentaciji*/}
              {this.state.fullPages.map((fp, i) =>
                <View key={fp.pageId} style={styles.presentation}>
                  <View style={styles.slideList}>
                    {fp.files.find(f => f.type == 'image') ?
                      <TouchableOpacity style={styles.wh100} onPress={() => {
                        Actions.HBF(
                          {
                            filtered: this.state.fullPages,
                            from: findMenuObjectById(this.state.fullPages[0].menuId),
                            selected: findMenu1Selected(findMenuObjectById(this.state.fullPages[0].menuId)),
                            indexStart: i,
                            presentationTitle: 'Presentation: ' + this.props.itsName,
                            whatIsOpen: 'Presentations',
                            backEnabled: true
                          })
                      }}>
                        <Image style={styles.presentationImg}
                          source={{ uri: 'file://' + RNFB.fs.dirs.DocumentDir + '/' + fp.files.find(f => f.type == 'image').filename }} />
                      </TouchableOpacity>
                      :
                      <TouchableOpacity style={styles.wh100} onPress={() => {
                        Actions.HBF(
                          {
                            filtered: this.state.fullPages,
                            from: findMenuObjectById(this.state.fullPages[0].menuId),
                            selected: findMenu1Selected(findMenuObjectById(this.state.fullPages[0].menuId)),
                            indexStart: i,
                            presentationTitle: 'Presentation: ' + this.props.itsName,
                            whatIsOpen: 'Presentations',
                            backEnabled: true
                          })
                      }}>
                        <Image style={styles.presentationImg} source={require('./ico/img/pres.jpg')} />
                      </TouchableOpacity>
                    }
                  </View>
                  <Text numberOfLines={2} style={styles.presentationTitle}>{fp.title || fp.subtitle}</Text>
                </View>

              )}

            </View>
          </ScrollView>

        </View>

      )
    } else {
      return (
        <View style={styles.content}>
          <View style={styles.navBtnCont}>
            <View style={styles.popoversCont}>
              <PopoverTouchable ref={m => { modal = m; }}>
                <TouchableOpacity disabled={this.state.sortableNotChecked} onPress={() => { }} style={styles.goBackBtn}><Image style={styles.headerImg} source={require('./ico/x64/trash.png')} /></TouchableOpacity>
                <Popover
                  visible={this.state.visibleDelete}
                  placement='bottom'
                  contentStyle={styles.deletePresPopover}
                  arrowStyle={styles.arrow}
                  backgroundStyle={styles.backgroundPopover}
                >

                  <Text style={[styles.popText, { marginBottom: 10 }]}>Are you sure you want to delete the selected items?</Text>

                  <View style={styles.deletePresPopoverBtnCont}>
                    <TouchableOpacity
                      onPress={() => modal.onClosePopover()}><Text style={styles.popText}>CANCEL</Text></TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => {
                        this.startDelete();
                        modal.onClosePopover();
                        this.savePresentation();
                      }}
                    >
                      <Text style={styles.popText}>DELETE</Text>
                    </TouchableOpacity>
                  </View>

                </Popover>
              </PopoverTouchable>

              <PopoverTouchable>
                <TouchableOpacity disabled={this.state.sortableNotChecked} style={styles.goBackBtn}><Image style={styles.headerImg} source={require('./ico/x64/add-64.png')} /></TouchableOpacity>
                <Popover
                  placement='bottom'
                  contentStyle={styles.addToOtherPresPopover}
                  arrowStyle={styles.arrow}
                  backgroundStyle={styles.backgroundPopover}
                >
                  <ScrollView showsVerticalScrollIndicator={false}>
                    <Text style={[styles.popText, { marginBottom: 5 }]}>Add to...</Text>
                    {this.props.svePrezentacije.map((p, i) => {
                      if (p.userId == this.props.userId && p.languageId == global.languageId && p.ime != this.props.itsName) {
                        return (
                          <View key={i} style={{ justifyContent: 'center' }}>
                            <View style={styles.addToOtherPresPopoverInnerCont}>
                              <TouchableOpacity style={{ marginRight: 10 }}
                                onPress={() => this.saveToOtherPresentation(this.state.fullPages, p.ime)}
                              >
                                <Image style={styles.popoverPresImg} source={this.whatIcon(p)} />
                              </TouchableOpacity>
                              <Text numberOfLines={1} style={[styles.popText, { width: '60%' }]}>{p.ime}</Text>
                            </View>

                          </View>
                        );
                      } else {
                        return;
                      }
                    })}
                  </ScrollView>
                </Popover>
              </PopoverTouchable>
            </View>
            <View style={styles.presTitleCont}>
              <Text style={styles.titleText}>{this.props.itsName ? this.props.itsName : ''}</Text>
            </View>

            <TouchableOpacity
              style={styles.goToEditBtn}
              onPress={() => {
                this.setState({ editToggle: !this.state.editToggle });
                this.savePresentation();
                this.saveToOtherPresentation();
              }}>
              <Image style={styles.goToEditImg} source={require('./ico/32/tick.png')} />
              <Text style={styles.goToEditText}>Done</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.wh100}>

            <SortableGrid
              dragStartAnimation={this.animacija()}
              blockTransitionDuration={200}
              activeBlockCenteringDuration={200}
              itemsPerRow={5}
              dragActivationTreshold={200}
              onDragRelease={({ itemOrder }) => {
                let a = itemOrder.map(p => this.state.fullPages.find(a => a.pageId == p.key));
                this.setState({ fullPages: [...a] });
              }}
              onDragStart={() => { }}
              ref={'SortableGrid'}
            >
              {
                this.state.fullPages.map((row, index) => {

                  return (
                    <View key={row.pageId}
                      style={styles.presentationView}
                      ref={'itemref_' + index}
                      onTap={() => this.magicTap(row.pageId)}>
                      <View style={styles.presentation} >
                        <View style={styles.slideListEdit}>
                          {this.renderOneSlide(row)}
                        </View>
                        <Text numberOfLines={2}
                          style={styles.presentationTitle}>{(row.title ? row.title : row.subtitle)}</Text>
                      </View>
                    </View>
                  )
                })
              }

            </SortableGrid>
          </View>


          {/* </ScrollView> */}

        </View>

      )
    }
  }
}

const styles = StyleSheet.create({
  PresentationCheckedCont: {
    width: '100%',
    height: '100%',
    position: 'relative',
    alignItems: 'flex-end',
    justifyContent: 'flex-end'
  },
  presentationImg: {
    position: 'relative',
    width: '100%',
    height: '100%'
  },
  tickImage: {
    height: 24,
    width: 24,
    bottom: 5,
    right: 5,
    position: 'absolute'
  },
  wh100: {
    width: '100%',
    height: '100%'
  },
  goBackBtn: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  goBackImg: {
    height: 15,
    width: 15,
    marginRight: 10
  },
  goBackText: {
    fontSize: 16,
    color: '#757575'
  },
  content: {
    width: '100%',
    height: '93%',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    position: 'absolute',
    flex: 1,
    zIndex: 3,
    marginLeft: 33
  },
  navBtnCont: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    width: '100%',
    zIndex: 5,
    marginTop: 15,
    marginRight: 10
  },
  PresNavBtnCont: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between'
  },
  titleText: {
    fontSize: 24
  },
  goToEditBtn: {
    alignSelf: 'flex-end',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  goToEditImg: {
    height: 15,
    width: 15,
    marginRight: 10
  },
  goToEditText: {
    fontSize: 16,
    color: '#757575'
  },
  presentationView: {
    width: '100%',
    height: '100%',
    flexWrap: 'wrap',
    flexDirection: 'row',
    alignSelf: 'center',
    paddingLeft: '3%',
    marginTop: 15
  },
  presentation: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    margin: 15,
  },
  slideList: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 170,
    height: 110,
  },
  presentationTitle: {
    width: 170,
    textAlign: 'center',
    paddingTop: 10,
    fontSize: 12
  },
  popoversCont: {
    width: '7%',
    justifyContent: 'space-between',
    flexDirection: 'row'
  },
  headerImg: {
    height: 20,
    width: 20,
    marginHorizontal: 5
  },
  deletePresPopover: {
    padding: 16,
    backgroundColor: '#757575',
    borderRadius: 8,
    width: 250
  },
  arrow: {
    borderTopColor: '#757575',
  },
  backgroundPopover: {
    backgroundColor: 'transparent'
  },
  popText: {
    color: 'white',
    fontSize: 16,
    textAlign: 'center'
  },
  deletePresPopoverBtnCont: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-around',
    marginTop: 10
  },
  addToOtherPresPopover: {
    padding: 16,
    backgroundColor: '#757575',
    borderRadius: 8,
    width: 350,
    maxHeight: 330,
  },
  addToOtherPresPopoverInnerCont: {
    flexDirection: 'row',
    marginVertical: 10,
    alignItems: 'center'
  },
  popoverPresImg: {
    width: 110,
    height: 70
  },
  presTitleCont: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  slideListEdit: {
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
    width: 170,
    height: 110,
  }
});
