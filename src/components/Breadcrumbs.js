import React, { Component } from 'react';
import { StyleSheet, TextInput, Text, View, TouchableOpacity, ImageBackground, Dimensions, TouchableHighlight } from 'react-native';
import _ from 'lodash';
import he from 'he';
import { Actions } from 'react-native-router-flux';
import { findMenuObjectById, findMenu1Selected } from '../../helpers';

export default class BreadcrumbsComponent extends Component {

  state = {
    menustr: '',
    "1": false,
    "2": false,
    "3": false,
    "4": false

  }

  findInfoAboutMenu = (menu) => {
    let filtered = global.globalJson.pages.filter(p => p.menuId == menu.menuId);
    let from = findMenuObjectById(menu.menuId);
    let selected = findMenu1Selected(from);
    return { filtered, from, selected };
  }

  searchMenu(menuId) {
    return global.globalJson.menus[0].menu.find(element =>
      menuId == element.menuId
    )
  }

  getBreadcrumb = (menuId, breadcrumb = '') => {
    let foundMenu = this.searchMenu(menuId);
    this.setState({ [foundMenu.depth]: foundMenu });
    if (foundMenu.depth == 1) {
      breadcrumb = foundMenu.title + breadcrumb;
      breadcrumb = he.decode(breadcrumb);
      return breadcrumb;
    } else {
      breadcrumb = '->' + foundMenu.title + breadcrumb;
      return this.getBreadcrumb(foundMenu.parentId, breadcrumb);
    }

  }

  componentWillReceiveProps(nextProps) {

    if (this.props.index !== nextProps.index) {
      this.setState({
        "1": false,
        "2": false,
        "3": false,
        "4": false
      })
      this.setState({
        menustr: this.getBreadcrumb(this.props.pages[nextProps.index].menuId)
      });
    }
  }

  componentWillMount() {
    if (this.props.pages[this.props.index]){
    this.setState({ menustr: this.getBreadcrumb(this.props.pages[this.props.index].menuId) });
    }
  }


  render() {
    return (
      <View style={styles.breadcrumbsContainer}>
        <View style={styles.breadcrumbsInnerContainer}>
          {this.state["1"] && <View style={[styles.breadcrumbsCont, {marginLeft: '2%'}]}><TouchableOpacity onPress={() => { }}><Text style={styles.textBreadCrumb}>{he.decode(this.state["1"].title)}</Text></TouchableOpacity></View>}

          {this.state["2"] && <View style={styles.breadcrumbsCont}><Text style={styles.textBreadCrumb}>   >   </Text><TouchableOpacity onPress={() => { let { filtered, selected, from } = this.findInfoAboutMenu(this.state["2"]); Actions.reset('HBF', { filtered, from, selected }); }}><Text style={styles.textBreadCrumb}>{he.decode(this.state["2"].title)}</Text></TouchableOpacity></View>}

          {this.state["3"] && <View style={styles.breadcrumbsCont}><Text style={styles.textBreadCrumb}>   >   </Text><TouchableOpacity onPress={() => { let { filtered, selected, from } = this.findInfoAboutMenu(this.state["3"]); Actions.reset('HBF', { filtered, from, selected }); }}><Text style={styles.textBreadCrumb}>{he.decode(this.state["3"].title)}</Text></TouchableOpacity></View>}

          {this.state["4"] && <View style={styles.breadcrumbsCont}><Text style={styles.textBreadCrumb}>   >   </Text><TouchableOpacity onPress={() => { let { filtered, selected, from } = this.findInfoAboutMenu(this.state["4"]); Actions.reset('HBF', { filtered, from, selected }); }}><Text style={styles.textBreadCrumb}>{he.decode(this.state["4"].title)}</Text></TouchableOpacity></View>}
        </View>
      </View>

    );
  }
}
const styles = StyleSheet.create({

  breadcrumbsContainer: {
    flex: 1,
    backgroundColor: '#f2f2f2',
    width: '100%',
    height: '7%',
    flexDirection: 'column',
    position: 'absolute',
    top: '7%',
    zIndex: 3,
  },
  breadcrumbsInnerContainer: {
    flexDirection: 'row',
    height: '100%',
    width: '100%',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  breadcrumbsCont: {
    flexDirection: 'row'
  },
  textBreadCrumb: {
    color: '#4a4a4a',
    fontSize: Dimensions.get('window').height * 0.03,
  },
});