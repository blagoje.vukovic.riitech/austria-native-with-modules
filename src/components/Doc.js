import React, { Component } from 'react';
import { StyleSheet, View, Image, TouchableOpacity, TouchableWithoutFeedback, Platform, WebView } from 'react-native';
import { Actions } from 'react-native-router-flux';
import PDF from 'react-native-pdf';

export default class DocumentView extends Component {

    componentWillMount() {
        clearTimeout(global.timer);
    }


    render() {
        return (
            <View style={styles.mainView}>
                <View style={styles.closeBtnCont}>
                    <TouchableOpacity activeOpacity={1} style={{ padding: 10, paddingHorizontal: 15 }} onPress={() => Actions.pop()}><Image style={styles.ico} source={require('./ico/32/back.png')} /></TouchableOpacity>
                </View>
                {this.props.file && <TouchableWithoutFeedback><View style={{
                    backgroundColor: 'black',
                    opacity: 1,
                    position: 'absolute',
                    top: 0,
                    zIndex: 1,
                    height: '100%',
                    width: '100%'
                }}></View></TouchableWithoutFeedback>}
                <View style={styles.pdfCont}>
                    {Platform.OS === 'ios' ?
                        <WebView
                            source={{ uri: this.props.docuri }}
                            style={{ flex: 12 }}
                        />
                        :
                        <PDF
                            fitPolicy={0}
                            source={{ uri: this.props.docuri }}
                            onLoadComplete={(pageCount, filePath) => {

                            }}
                            onPageChanged={(page, pageCount) => {

                            }}
                            onError={(error) => {

                            }}
                            style={styles.pdf} />
                    }
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        width: '100%',
        height: '100%'
    },
    closeBtnCont: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'flex-end',
        backgroundColor: '#eeeeee'
    },
    ico: {
        height: 25,
        width: 25,
    },
    pdfCont: {
        flex: 15
    },
    pdf: {
        width: '100%',
        height: '100%'
    }
});