import React, { Component } from 'react';
import { StyleSheet, View, Image, TouchableOpacity, TextInput, Keyboard, KeyboardAvoidingView, Text, NetInfo, Platform, Alert, AsyncStorage, ActivityIndicator, ScrollView } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import md5 from 'md5';
import RNFB from 'react-native-fetch-blob';
import { isNetworkConnected } from '../../helpers';

export default class SignUpModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstname: '',
      surname: '',
      email: '',
      password: '',
      error: '',
      isConnected: false,
      msg: '',
      spinner: false,
      keyboard: false
    };
  }

  registerUser() {
    const { firstname, surname, email, password } = this.state;
    if (email === '' || password === '') {
      this.setState({
        spinner: false
      })
      Alert.alert(
        '',
        'Email and password are mandatory!',
        [
          { text: 'Ok', onPress: () => { } }
        ]
      );
    } else if (email.match(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*?\.[a-zA-Z]{2,3}$/) === null) {
      this.setState({
        spinner: false
      })
      Alert.alert(
        '',
        'Invalid email format',
        [
          { text: 'Ok', onPress: () => { } }
        ]
      );
    } else {
      const formData = new FormData();
      formData.append("firstname", firstname);
      formData.append("surname", surname);
      formData.append("email", email);
      formData.append("password", md5(password));
      console.log(formData);
      fetch(global.server + '?a=ajax&do=registerUser&projectId=' + global.projectId + '&token=' + global.jsonToken + global.userToken, {
        method: 'POST',
        body: formData
      })
        .then(response => {
          console.log(response)
          res = JSON.parse(response._bodyText);
          if (res.hasOwnProperty("userId")) {
            this.setState({ firstname: '', surname: '', email: '', password: '', spinner: false });
            Alert.alert(
              'You have registered successfully',
              'You have to log in to proceed',
              [
                { text: 'Log In', onPress: () => { this.props.changeToLogin(); this.myLoop(); } },
                // { text: 'Cancel', onPress: () => {} }
              ]
            )

          } else {
            this.setState({
              spinner: false
            })
            Alert.alert(
              '',
              ('Entered ' + res.resultText + ' in the database!'),
              [
                { text: 'Ok', onPress: () => { } }
              ]
            );
          }
        })
        .catch(error => console.log(error));
    }
  }

  myLoop = () => {
    return new Promise((resolve, reject) => {
      setTimeout(() => { this.fetchUserJson().then(() => resolve()).catch((err) => { console.log(err); this.myLoop(); return reject(); }) }, 2000);

    })
  }

  fetchUserJson = () => {
    console.log('okinuo fetchUserJson()');
    return new Promise((resolve, reject) => {
      fetch(global.server + '?a=ajax&do=getUsers&projectId=' + global.projectId + '&token=' + global.jsonToken + global.userToken)
        .then(res => res.json())
        .then(res => {
          console.log(res.lastChanges);
          if (res.lastChanges == global.usersJson.lastChanges) {
            console.log('i dalje je stari online');
            return Promise.reject('Nije stigao novi json');
          } else {
            console.log('stigao novi');
            AsyncStorage.setItem('usersJson', JSON.stringify(res))
              .then(() => {
                global.usersJson = res;
                return Promise.resolve('stigao novi');
              })
          }
        })
        .then(() => resolve())
        .catch((err) => { console.log(err); return reject() })
    })
  }

  signupOrSpinner = () => {
    if (this.state.spinner) {
      return (
        <View style={styles.registerCont}>
          <ActivityIndicator color='#BDB9B9' size='large' />
        </View>
      );
    }
    return (
      <View style={styles.registerCont}>
        <TouchableOpacity style={[styles.buttonSignUp, { backgroundColor: this.state.isConnected ? '#d8d8d8' : '#BDB9B9' }]} onPress={() => { this.setState({ spinner: true }); this.registerUser() }} disabled={!this.state.isConnected}>
          <Text style={[styles.buttonText, { color: this.state.isConnected ? '#424242' : 'white' }]}>SIGN UP</Text>
        </TouchableOpacity>
      </View>
    );
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = () => {
    this.setState({ keyboard: true })
  }

  _keyboardDidHide = () => {
    this.setState({ keyboard: false })
  }

  componentWillMount() {
    isNetworkConnected()
      .then(res => {
        this.setState(() => ({ isConnected: res }));
        return Promise.resolve();
      })
      .then(() => {
        if (this.state.isConnected === false) {
          this.setState({ msg: 'No internet connection, you can not register at the moment!' })
        }
      })
      .catch(error => console.log(error));
  }


  render() {
    return (
      <View style={styles.container}>

        

        <KeyboardAvoidingView behavior='padding'
          style={styles.avoidScroll}
        >
          <View style={styles.inputCont}>
          <Text style={styles.noInternetText}>{this.state.msg}</Text>
            <Text style={styles.inputTitle}>E-MAIL</Text>
            <TextInput style={styles.inputBox}
              underlineColorAndroid='white'
              keyboardType="email-address"
              returnKeyType="next"
              value={this.state.email}
              onChangeText={email => this.setState({ email })}
              onSubmitEditing={() => this.password.focus()}
              editable={this.state.isConnected}
            />
            <Text style={styles.inputTitle}>PASSWORD</Text>
            <TextInput style={styles.inputBox}
              underlineColorAndroid='white'
              secureTextEntry={true}
              returnKeyType="next"
              ref={(input) => this.password = input}
              value={this.state.password}
              onChangeText={password => this.setState({ password })}
              onSubmitEditing={() => this.firstname.focus()}
              editable={this.state.isConnected}
            />
            <Text style={styles.inputTitle}>FIRST NAME</Text>
            <TextInput style={styles.inputBox}
              underlineColorAndroid='white'
              returnKeyType="next"
              ref={(input) => this.firstname = input}
              value={this.state.firstname}
              onChangeText={firstname => this.setState({ firstname })}
              onSubmitEditing={() => this.surname.focus()}
              editable={this.state.isConnected}
            />
            <Text style={styles.inputTitle}>SURNAME</Text>
            <TextInput style={styles.inputBox}
              underlineColorAndroid='white'
              returnKeyType="next"
              value={this.state.surname}
              onChangeText={surname => this.setState({ surname })}
              ref={(input) => this.surname = input}
              editable={this.state.isConnected}
            />

          </View>

          {this.signupOrSpinner()}
        
          {!this.state.keyboard &&
            <View style={styles.buttonCont}>
              <View style={styles.signupTC}>
                <TouchableOpacity onPress={() => this.props.changeToLogin()}><Text style={styles.tekst}>ALREADY HAVE AN ACCOUNT?</Text></TouchableOpacity>
              </View>
            </View>
          }
       </KeyboardAvoidingView>
      </View>

    )
  }
}

const styles = StyleSheet.create({
  container: {
    marginLeft: 'auto',
    marginRight: 'auto',
    height: '100%',
    width: '100%',
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  noInternetText: {
    fontSize: 24,
    fontWeight: '700',
    textAlign: 'center',
    color: 'red',
    marginBottom: 30
  },
  avoidScroll: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%'
  },
  inputCont: {
    height: '60%',
    alignItems: 'center',
    justifyContent: 'flex-end',
    flexDirection: 'column',
    width: '80%',
    marginTop: 20
  },
  inputTitle: {
    alignSelf: 'flex-start',
    fontSize: 14
  },
  inputBox: {
    paddingVertical: 0.1,
    width: '100%',
    height: 30,
    backgroundColor: "white",
    borderRadius: 5,
    fontSize: 18,
    color: "#757575",
    margin: 10,
    borderBottomWidth: 2,
    borderColor: "#d8d8d8"
  },
  registerCont: {
    height: '20%',
    width: '50%',
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'column'
  },
  buttonSignUp: {
    backgroundColor: 'blue',
    width: '100%',
    height: '50%',
    justifyContent: 'center',
    marginTop: 10
  },
  buttonText: {
    fontSize: 20,
    fontWeight: '100',
    textAlign: 'center',
  },
  buttonCont: {
    height: '20%',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    width: '80%'
  },
  signupTC: {
    width: '100%',
    alignItems: 'center'
  },
  tekst: {
    color: "#959A9C",
    fontSize: 16
  }
});
