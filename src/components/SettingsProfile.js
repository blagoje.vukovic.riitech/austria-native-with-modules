import React, { Component } from 'react';
import { View, Text, StyleSheet, AsyncStorage } from 'react-native';

class SettingsProfile extends Component {

    state = {
        user: null
    }

    componentDidMount() {
        AsyncStorage.getItem('@userId')
            .then((res) => {
                let a = global.usersJson.users.find(u => u.userId == res);
                this.setState({ user: a });
            })
    }

    render() {

        if (this.state.user) {
            return (
                <View style={styles.mainContainer}>
                    <View style={styles.profileItemCont}>
                        <Text style={styles.infoText}>EMAIL:</Text>
                        <Text style={styles.infoText2}>{this.state.user.email}</Text>
                    </View>
                    <View style={styles.profileItemCont}>
                        <Text style={styles.infoText}>FIRST NAME:</Text>
                        <Text style={styles.infoText2}>{this.state.user.firstName || ''}</Text>
                    </View>
                    <View style={styles.profileItemCont}>
                        <Text style={styles.infoText}>LAST NAME:</Text>
                        <Text style={styles.infoText2}>{this.state.user.lastName || ''}</Text>
                    </View>
                </View>
            )
        } else {
            return null;
        }
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    profileItemCont: {
        flexDirection: 'row', 
        marginLeft: '20%'
    },
    infoText: {
        fontFamily: 'Raleway-Regular',
        fontSize: 25,
        margin: 30,
        color: '#9e9e9e'
    },
    infoText2: {
        fontFamily: 'Raleway-SemiBold',
        fontSize: 28,
        marginTop: 30
    }
});


export default SettingsProfile;