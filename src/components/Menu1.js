import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Dimensions } from 'react-native';
import HTML from 'react-native-render-html';

class Menu1 extends Component {

    render() {

        return (

            <View>
                <View style={styles.mainMenu1}>
                    <TouchableOpacity onPress={this.props.onPress} style={[styles.menu1Item, { backgroundColor: this.props.isPressed ? '#da291c' : '#d8d8d8', width: Dimensions.get('window').width / this.props.i }]}>
                        <HTML containerStyle={styles.menu1TextCont} baseFontStyle={{color: this.props.isPressed ? 'white' : '#757575', fontSize: 12, textAlign: 'left' }} html={this.props.menu1.title} />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = {
    mainMenu1: {
        paddingTop: 0,
    },
    menu1Item: {
        marginLeft: 0,
        marginRight: 0,
        borderRightWidth: 1,
        borderColor: '#fff',
        height: 50,
        // width: 300,
        alignItems: 'flex-start',
        justifyContent: 'center',
        padding: 10
    },
    menu1TextCont: { 
        paddingLeft: 5
    }
}

export default Menu1;