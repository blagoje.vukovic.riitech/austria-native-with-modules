import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, Dimensions, StatusBar, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import VideoTour from './VideoTour';
import Modall from './Modall';
import LeafletButton from './LeafletButton';
import { renderVB, renderDB, renderModalforMultipleFiles, renderModalPresentation } from '../../helpers';
import { LeafletsModule, PresentationModule, VideoTourModule } from '../../ModuleConfig'

let LeafletsOrPres = LeafletsModule || PresentationModule;

export default class ImageButtons extends Component {

    state = {
        videoPath: [],
        documentPath: [],
        image: '',
        whichOne: '',
        visableTwoBtns: false,
        modalVisible: false,
        videos: false,
        documents: false
    };

    componentWillMount() {
        if (this.props.page.files) {
            let videos = this.props.page.files.filter(file => file.type == 'video')

            let documents = this.props.page.files.filter(file => file.type == 'document');

            let images = this.props.files.find(file => {
                return file.substring(file.length - 3, file.length) == 'jpg'
                    || file.substring(file.length - 3, file.length) == 'png'
                    || file.substring(file.length - 4, file.length) == 'jpeg'
            })


            this.setState({ videoPath: videos, documentPath: documents, image: images });
        }
    }
    componentDidMount() {
        StatusBar.setHidden(true);
    }

    hideModal = () => {
        this.setState({ videos: false, documents: false });
    }

    showModal = (which) => {
        this.setState({ [which]: true });
    }

    render() {
        console.log(this.state.image)
        return (

            <View style={styles.mainView}>
                {(!this.props.fromHome && !this.props.start && LeafletsOrPres) && <LeafletButton changed={this.props.changed} page={this.props.page} />}
                <View style={styles.body}>
                {this.props.home && VideoTourModule && <VideoTour open={()=> this.setState({videos: 2})}/>}
                    <View style={{ flex: 0.8 }}>
                        <Text style={[styles.headingText, styles.headingMain]}>{this.props.templateTitle}</Text>
                        <Text style={styles.headingText}>{this.props.subtitle}</Text>
                    </View>
                    <View style={styles.contentContainer}>

                        <View style={styles.contentPic}>
                            <Modall>
                                <Image resizeMethod='resize' style={styles.contentPicImg} source={{ uri: this.state.image }} />
                            </Modall>
                            <View style={styles.ButtonContainer}>
                                {renderVB(this.state.videoPath, this.showModal.bind(null, 'videos'))}
                                {renderDB(this.state.documentPath, this.showModal.bind(null, 'documents'))}
                            </View>
                        </View>

                    </View>

                </View>
                {renderModalforMultipleFiles('videos', this.state.videoPath, this.state.videos, this.hideModal)}
                {renderModalforMultipleFiles('documents', this.state.documentPath, this.state.documents, this.hideModal)}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    mainView: {
        backgroundColor: 'white',
        position: 'absolute',
        height: '100%'
    },
    body: {
        height: '100%'
    },
    headingText: {
        color: '#494949',
        fontSize: 15,
        paddingBottom: 10,
        paddingLeft: 30,
    },
    headingMain: {
        paddingTop: 20,
        paddingBottom: 10,
        fontSize: 22,
        fontWeight: 'bold'
    },
    contentContainer: {
        marginTop: 20,
        flexDirection: 'row',
        width: '100%',
        height: '100%',
        marginBottom: 10,
        position: 'relative',
        flex: 5
    },
    contentPic: {
        width: '100%',
        height: '95%',
        backgroundColor: 'white',
        position: 'relative',
        alignItems: 'center'
    },
    contentPicImg: {
        width: '100%',
        height: '100%',
        resizeMode: 'cover'
    },
    ButtonContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        width: '100%',
        height: '10%',
        marginRight: 5
    }
});

