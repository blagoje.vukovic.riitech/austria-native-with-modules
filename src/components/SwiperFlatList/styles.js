import { StyleSheet } from 'react-native';

import { vertical, horizontal, width } from '../../themes';

export default StyleSheet.create({
  paginationContainer: {
    position: 'absolute',
    flexDirection: 'row',
    marginVertical: vertical.xxSmall,
    justifyContent: 'center',
    bottom: -8,
    backgroundColor: 'transparent',
    paddingVertical: 4,
    width: '100%',
    overflow: 'hidden'

  },
  pagination: {
    width: 14,
    height: 14,
    marginHorizontal: 4,
    borderRadius: 7,
    borderWidth: 1,
    borderColor: '#9B9B9B'
  },
});
