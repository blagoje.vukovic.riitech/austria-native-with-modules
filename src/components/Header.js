import React, { Component } from 'react';
import { StyleSheet, View, Image, StatusBar, TouchableWithoutFeedback, TouchableOpacity, TextInput, Alert, Text, AsyncStorage, ActivityIndicator } from 'react-native';
import { Actions } from 'react-native-router-flux';
import HTML from 'react-native-render-html';
import RNRestart from 'react-native-restart';
import RNFB from 'react-native-fetch-blob';
import Modal from "react-native-modal";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { isNetworkConnected } from '../../helpers';
import { LeafletsModule, MediaCenterModule, PresentationModule, BreadCrumbsModule, VideoCenterModule, LanguageModule, DashboardModule, NotificationModule } from '../../ModuleConfig'


export default class Header extends Component {

  state = {
    isConnected: true,
    refresh: true
  };

  openLanguage = () => {
    this.props.onPressLang();
  };

  openHome = () => {
    Actions.reset('home', { lang: global.language });
  };

  openFavorites = () => {
    Actions.reset('login')
  };

  openPresentation = () => {
    this.props.onPressPresentation();
  };

  openSearch = () => {
    this.props.onPressSearch();
  };

  openSettings = () => {
    this.props.onPressSettings();
  };

  openDashboard = () => {
    this.props.onPressDashboard();
  };

  openBreadcrumbs = () => {
    this.props.onPressBreadcrumbs();
  };

  openLeaflets = () => {
    this.props.onPressLeaflets();
  };

  openMediaCenter = () => {
    this.props.onPressMediaCenter();
  };

  openVideos = () => {
    this.props.onPressVideos();
  };

  openVideoTour = () => {
    this.props.onPressVideos();
  };

  openNotification = () => {
    this.props.onPressNotification();
  };

  openMagna = () => {
    alert('This is Magna! :)')
  };

  divider = () => {
    alert('This is Magna! :)')

  };

  componentWillMount() {
    isNetworkConnected()
      .then(res => res ? this.setState({ isConnected: true }) : this.setState({ isConnected: false }));
  }

  refresh = () => {
    this.setState({ refresh: !this.state.refresh });
  }

  componentDidMount() {
    StatusBar.setHidden(true);
  }


  render() {

    return (

      <View style={styles.navbarH}>
        <StatusBar barStyle="dark-content" hidden={true} />
        <View style={styles.headerCont}>
          {this.props.backEnabled && <TouchableOpacity style={styles.goBackBtn} onPress={() => Actions.pop()}><Image style={styles.headerImg} source={require('./ico/img/presentationLeft.png')} /><Text style={styles.headerText}>Go back</Text></TouchableOpacity>}
          <View style={styles.headTitleCont}>{(!this.props.home || this.props.whatIsOpen != '') && <HTML baseFontStyle={{ fontSize: this.props.backEnabled ? 18 : 14 }} html={this.props.whatIsOpen ? this.props.whatIsOpen : this.props.title} />}</View>
          <View style={styles.btnCont}>
            {DashboardModule &&
              <View style={{ height: 30, width: 32, justifyContent: 'center', alignItems: 'center', marginLeft: 10 }}>
                <TouchableWithoutFeedback onPress={this.openDashboard}><Image style={styles.ico} source={this.props.whatIsOpen != 'Dashboard' ? require('./ico/top-bar/dashboard.png') : require('./ico/top-bar/dashboard_pressed.png')} /></TouchableWithoutFeedback>
              </View>}
              {LanguageModule &&
            <View style={{ height: 50, width: 52, justifyContent: 'center', alignItems: 'center', marginLeft: 10 }}>
              <TouchableOpacity activeOpacity={1} style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' }} onPress={this.openLanguage}><Image style={styles.icoLang} source={this.props.whatIsOpen != 'Language' ? require('./ico/top-bar/language.png') : require('./ico/top-bar/language_pressed.png')} /></TouchableOpacity>
            </View>}
            {BreadCrumbsModule && 
            <View style={{ height: 30, width: 32, justifyContent: 'center', alignItems: 'center', marginLeft: 10 }}>
              <TouchableWithoutFeedback onPress={this.openBreadcrumbs}><Image style={styles.icoBread} source={this.props.whatIsOpen != 'Breadcrumbs' ? require('./ico/top-bar/breadcrumbs.png') : require('./ico/top-bar/breadcrumbs_pressed.png')} /></TouchableWithoutFeedback>
            </View>
            }
            <View style={{ height: 35, width: 5, justifyContent: 'center', alignItems: 'center', marginLeft: 10 }}>
              <TouchableWithoutFeedback disabled={true} onPress={this.divider}><Image style={styles.icoDiv} source={require('./ico/x64/divider.png')} /></TouchableWithoutFeedback>
            </View>
            {PresentationModule && 
            <View style={{ height: 30, width: 32, justifyContent: 'center', alignItems: 'center', marginLeft: 10 }}>
              <TouchableWithoutFeedback disabled={this.props.backEnabled ? true : false} onPress={this.openPresentation}><Image style={styles.ico_smaller_2} source={this.props.whatIsOpen != 'Presentation' ? require('./ico/top-bar/presentation.png') : require('./ico/top-bar/presentation_pressed.png')} /></TouchableWithoutFeedback>
            </View>}
            {LeafletsModule &&
            <View style={{ height: 30, width: 32, justifyContent: 'center', alignItems: 'center', marginLeft: 10 }}>
              <TouchableWithoutFeedback onPress={this.openLeaflets}><Image style={styles.ico_smaller} source={this.props.whatIsOpen != 'Leaflets' ? require('./ico/top-bar/leaflets.png') : require('./ico/top-bar/leaflets_pressed.png')} /></TouchableWithoutFeedback>
            </View>}
            <View style={{ height: 50, width: 52, justifyContent: 'center', alignItems: 'center', marginLeft: 10 }}>
              <TouchableOpacity activeOpacity={1} style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' }} onPress={this.openSearch}><Image style={styles.ico_smaller_44} source={this.props.whatIsOpen != 'Search' ? require('./ico/top-bar/search.png') : require('./ico/top-bar/search_pressed.png')} /></TouchableOpacity>
            </View>
            {VideoCenterModule && 
            <View style={{ height: 30, width: 32, justifyContent: 'center', alignItems: 'center', marginLeft: 10 }}>
              <TouchableWithoutFeedback onPress={this.openVideos}><Image style={styles.ico_smaller_44} source={this.props.whatIsOpen != 'Videos' ? require('./ico/top-bar/videos.png') : require('./ico/top-bar/videos_pressed.png')} /></TouchableWithoutFeedback>
            </View>}
            {MediaCenterModule && 
            <View style={{ height: 30, width: 32, justifyContent: 'center', alignItems: 'center', marginLeft: 10 }}>
              <TouchableWithoutFeedback onPress={this.openMediaCenter}><Image style={styles.ico_smaller_3} source={this.props.whatIsOpen != 'Mediacenter' ? require('./ico/top-bar/media_center.png') : require('./ico/top-bar/media_center_pressed.png')} /></TouchableWithoutFeedback>
            </View>}
            {NotificationModule && 
            <View style={{ height: 30, width: 32, justifyContent: 'center', alignItems: 'center', marginLeft: 10 }}>
              <TouchableWithoutFeedback onPress={this.openNotification}><Image style={styles.ico_smaller_4} source={this.props.whatIsOpen != 'Notifications' ? require('./ico/top-bar/notifications.png') : require('./ico/top-bar/notifications_pressed.png')} /></TouchableWithoutFeedback>
            </View>}
            <View style={{ height: 50, width: 52, justifyContent: 'center', alignItems: 'center', marginLeft: 10 }}>
              <TouchableOpacity activeOpacity={1} style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' }} onPress={this.openSettings}><Image style={styles.ico_smaller_44} source={this.props.whatIsOpen != 'Settings' ? require('./ico/top-bar/settings.png') : require('./ico/top-bar/settings_pressed.png')} /></TouchableOpacity>
            </View>
            <View style={{ height: 35, width: 5, justifyContent: 'center', alignItems: 'center', marginLeft: 10 }}>
              <TouchableWithoutFeedback disabled={true} onPress={this.divider}><Image style={styles.icoDiv} source={require('./ico/x64/divider.png')} /></TouchableWithoutFeedback>
            </View>
            <View style={{ height: 50, width: 52, justifyContent: 'center', alignItems: 'center', marginLeft: 10 }}>
              <TouchableWithoutFeedback activeOpacity={1} style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' }} onPress={this.openHome}><Image style={styles.ico} source={this.state.isConnected ? require('./ico/x64/magna.png') : require('./ico/x64/magna_offline.png')} /></TouchableWithoutFeedback>
            </View>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  navbarH: {
    height: '7%',
    width: '100%',
    backgroundColor: '#fff',
    justifyContent: "center",
    flexDirection: 'row',
    paddingRight: 10,
    borderBottomWidth: 1,
    borderColor: '#dddddd'
  },
  headerCont: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  goBackBtn: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginLeft: 10
  },
  headerImg: {
    height: 20,
    width: 20,
    marginHorizontal: 10
  },
  headerText: {
    fontSize: 18,
  },
  headTitleCont: {
    flex: 1,
    alignItems: 'center',
    alignSelf: 'center',
    width: '100%'
  },
  btnCont: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  ico: {
    height: 30,
    width: 30,

  },
  icoLang: {
    height: 24,
    width: 24,

  },
  icoBread: {
    height: 22,
    width: 22,

  },
  icoDiv: {
    height: 30,
    width: 24,

  },
  ico_smaller: {
    height: 26.4,
    width: 19,

  },
  ico_smaller_2: {
    height: 20,
    width: 25.42,

  },
  ico_smaller_3: {
    height: 20,
    width: 31.4,

  },
  ico_smaller_4: {
    height: 24,
    width: 25.5,

  },
  ico_smaller_44: {
    height: 26.4,
    width: 26.4,

  }
});
