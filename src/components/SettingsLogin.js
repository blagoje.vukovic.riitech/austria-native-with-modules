import React, { Component } from 'react';
import { StyleSheet, View, Image, TouchableOpacity, KeyboardAvoidingView, TextInput, Text, NetInfo, Platform, AsyncStorage, Alert, ScrollView, ActivityIndicator } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import md5 from 'md5';
import { Actions } from 'react-native-router-flux';
import { isNetworkConnected } from '../../helpers';

export default class SettingsLogin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      error: '',
      isChecked: true,
      isConnected: false,
      userId: '',
      spinner: false
    };

  }

  logIn() {
    const users = global.usersJson.users;
    const hashPass = md5(this.state.password);
    if (this.state.isConnected === false) {
      user = users.find(({ email, password }) => {
        return email === this.state.email && password === hashPass
      });
      if (user === undefined) {
        this.setState({
          email: '',
          password: '',
          spinner: false
        });
        Alert.alert(
          '',
          'Incorrect username or password',
          [
            { text: 'Ok', onPress: () => { } }
          ]
        )
      } else if (user.type == 0) {
        this.setState({
          userId: user.userId,
          email: '',
          password: '',
          spinner: false
        });
        this.setLoggedUser(user.userId);
        Alert.alert(
          'SUCCESS!',
          'You have logged in successfully',
          [
            { text: 'OK', onPress: () => this.props.onChange() },
          ],
          { cancelable: false }
        )
      } else {
        this.setState({
          spinner: false
        });
        Alert.alert(
          'ERROR!',
          'Please contact our support.',
          [
            { text: 'OK', onPress: () => { } },
          ],
          { cancelable: false }
        )
      }
    } else {
      let formData = new FormData();
      formData.append("email", this.state.email);
      formData.append("password", hashPass);
      fetch(global.server + '?a=ajax&do=loginUser&projectId=' + global.projectId + '&token=' + global.jsonToken + global.userToken, {
        method: 'POST',
        body: formData
      })
        .then(response => {
          console.log(response)
          res = JSON.parse(response._bodyText);
          if (res.hasOwnProperty("userId")) {
            this.setState({
              userId: res.userId,
              email: '',
              password: '',
              spinner: false
            });
            this.setLoggedUser(res.userId, res.userToken);
            Alert.alert(
              'SUCCESS!',
              'You have logged in successfully',
              [
                { text: 'OK', onPress: () => this.props.onChange() }
              ],
              { cancelable: false }
            )
          } else {
            this.setState({
              email: '',
              password: '',
              spinner: false
            });
            Alert.alert(
              '',
              'Incorrect username or password',
              [
                { text: 'Ok', onPress: () => { } }
              ]
            );
          }
        })
        .catch(error => this.setState({ error }));
    }
  }

  loginOrSpinner = () => {
    if (this.state.spinner) {
      return (
        <View style={styles.buttonLog}>
          <ActivityIndicator color='#BDB9B9' size='large' />
       </View>
      );
    }
    return (
      <TouchableOpacity style={[styles.buttonLog, { backgroundColor: this.state.isChecked ? '#BDB9B9' : '#d8d8d8' }]} onPress={ () => { this.setState({spinner: true}); this.logIn()}} disabled={this.state.isChecked}>
        <Text style={[styles.buttonText, { color: this.state.isChecked ? 'white' : '#424242' }]}>LOGIN</Text>
      </TouchableOpacity>
    );

  }

  enableLogin() {
    const { email, password } = this.state;
    if (email !== '' && password !== '') {
      this.setState({ isChecked: false });
    } else if (email === '' || password === '') {
      this.setState({ isChecked: true });
    } else {
      this.setState({ isChecked: true })
    }
  }

  setLoggedUser(userId) {
    console.log('usao u setloggeduser')
    AsyncStorage.setItem('@userId', userId);
    AsyncStorage.setItem('@userToken', userToken);
    global.userToken = '&userToken=' + userToken;
    console.log('Success write to AsyncStorage');
  }


  componentWillMount() {
    isNetworkConnected()
      .then(res => {
        this.setState(() => ({ isConnected: res }));
        return Promise.resolve();
      })
      .then(() => { console.log(this.state.isConnected) })
      .catch(error => console.log(error));
  }


  render() {
    return (
      <View style={styles.container}>

      <KeyboardAvoidingView style={styles.keyboardAware} behavior='padding'>
        <View style={styles.inputCont}>

          <Text style={styles.inputTitle}>USERNAME</Text>

          <TextInput style={styles.inputBox}
            underlineColorAndroid='white'
            keyboardType="email-address"
            returnKeyType="next"
            value={this.state.email}
            onChangeText={email => { this.enableLogin(); this.setState({ email }) }}
            onSubmitEditing={() => this.password.focus()}
          />
          <Text style={styles.inputTitle}>PASSWORD</Text>
          <TextInput style={styles.inputBox}
            underlineColorAndroid='white'
            secureTextEntry={true}
            returnKeyType="go"
            value={this.state.password}
            onChangeText={password => { this.enableLogin(); this.setState({ password }) }}
            ref={(input) => this.password = input}
          />

        </View>
        <View style={styles.login}>

        {this.loginOrSpinner()}
       
          <TouchableOpacity style={styles.buttonReg} onPress={() => this.props.changeToSignUp()}>
            <Text style={styles.buttonText}>SIGN UP</Text>
          </TouchableOpacity>

        </View>
        </KeyboardAvoidingView>
        <View style={styles.bottomCont}>
          <View style={styles.skip}>
            <TouchableOpacity onPress={() => this.props.changeToForgotPassword()} style={styles.skipBtn}><Text style={styles.tekst}>FORGOT PASSWORD</Text></TouchableOpacity>
          </View>
        </View>

      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
   
    marginLeft: 'auto',
    marginRight: 'auto',
    height: '100%',
    width: '100%',
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1
  },
  keyboardAware: {
    width: '100%', 
    height: '100%', 
    alignItems: 'center', 
    flex: 8
  },
  inputCont: {
    flex: 4,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    width: '70%'
  },
  inputTitle: {
    alignSelf: 'flex-start',
    fontSize: 16
  },
  inputBox: {
    width: '100%',
    height: 40,
    backgroundColor: "white",
    borderRadius: 5,
    fontSize: 18,
    color: "#757575",
    margin: 10,
    borderBottomWidth: 2,
    borderColor: "#d8d8d8"
  },
  login: {
    flex: 4,
    width: '60%',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column'
  },
  buttonLog: {
    width: '90%',
    height: '30%',
    justifyContent: 'center',
    marginBottom: 15
  },
  buttonText: {
    fontSize: 20,
    fontWeight: '100',
    color: "#424242",
    textAlign: 'center',
  },
  buttonReg: {
    backgroundColor: 'white',
    borderWidth: 2,
    borderColor: '#d8d8d8',
    width: '90%',
    height: '30%',
    justifyContent: 'center',
  },
  bottomCont: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    width: '60%'
  },
  skip: {
    alignItems: 'flex-end',
    backgroundColor: 'white',
  },
  skipBtn: {
    alignItems: 'center',
    paddingTop: 25,
  },
  tekst: {
    color: "#959A9C",
    fontSize: 16
  },
});