import React, { Component } from 'react';
import { StyleSheet, View, Image } from 'react-native';
import LeafletButton from './LeafletButton';
import VideoTour from './VideoTour';
import { renderVB, renderDB, renderModalforMultipleFiles, renderModalPresentation } from '../../helpers';
import { LeafletsModule, PresentationModule, VideoTourModule } from '../../ModuleConfig'

let LeafletsOrPres = LeafletsModule || PresentationModule;

export default class FullImage extends Component {

  state = {
    videoPath: [],
    documentPath: [],
    videos: false,
    documents: false,
  }

  componentWillMount() {
    if (this.props.page.files) {
      let videos = this.props.page.files.filter(file => file.type == 'video')

      let documents = this.props.page.files.filter(file => file.type == 'document');

      this.setState({ videoPath: videos, documentPath: documents });
    }
  }

  hideModal = () => {
    this.setState({ videos: false, documents: false });
  }

  showModal = (which) => {
    this.setState({ [which]: true });
  }

  render() {

    return (
      <View style={styles.mainView}>
        {(!this.props.fromHome && !this.props.start && LeafletsOrPres) && <LeafletButton changed={this.props.changed} page={this.props.page} />}

        <View style={styles.body}>
        {this.props.home && VideoTourModule && <VideoTour open={()=> this.setState({videos: 2})}/>}
          <View style={styles.contentContainer}>

            <View style={styles.contentPic}>

              <Image resizeMethod='resize' style={styles.contentPicImg} source={{ uri: this.props.files[0] }} />

              <View style={styles.ButtonContainer}>
                {renderVB(this.state.videoPath, this.showModal.bind(null, 'videos'))}
                {renderDB(this.state.documentPath, this.showModal.bind(null, 'documents'))}
              </View>

            </View>

          </View>

        </View>
        {renderModalforMultipleFiles('videos', this.state.videoPath, this.state.videos, this.hideModal)}
        {renderModalforMultipleFiles('documents', this.state.documentPath, this.state.documents, this.hideModal)}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainView: {
    backgroundColor: 'white',
    position: 'relative',
    height: '100%',
  },
  body: {
    height: '100%',
  },
  contentContainer: {
    flexDirection: 'row',
    flex: 1,
    width: '100%',
    height: '100%',
  },
  contentPic: {
    flex: 3,
    height: '100%',
    backgroundColor: 'white',
  },
  contentPicImg: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover'
  },
  ButtonContainer: {
    justifyContent: 'flex-end',
    alignItems: 'center',
    flexDirection: 'row',
    position: 'absolute',
    bottom: 25,
    right: 25,
    width: '30%',
    height: 10
  }
});
