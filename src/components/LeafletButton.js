import React, { Component } from 'react';
import { Text, View, TouchableOpacity, AsyncStorage, Image, StyleSheet } from 'react-native';
import _ from 'lodash';
import { Actions } from 'react-native-router-flux';
import Modal from 'react-native-modal';
import PM from './PresentationModal';
import { LeafletsModule, PresentationModule } from '../../ModuleConfig'

export default class LeafletButton extends Component {


    state = {
        visibleTwoBtns: false,
        isActiveMainButton: false,
        isAlreadyInLeaflet: false,
        i: -1,
        modalVisible: false,
        prezentacije: []
    };


    addToLeaflet = () => {
        if (this.checkIfAlreadyInLeaflet()) {
            _.remove(global.leaflets.fullPages, (p) => p.pageId == this.props.page.pageId);
        } else {
            global.leaflets.fullPages.push(this.props.page);
        }
        AsyncStorage.setItem('leaflets', JSON.stringify(global.leaflets));
        this.checkIfAlreadyInLeaflet();
    }

    checkIfAlreadyInLeaflet = () => {
        let currentPageId = this.props.page.pageId;
        let index = global.leaflets.fullPages.findIndex(p => p.pageId == currentPageId);

        if (index == -1) {
            this.setState({ isAlreadyInLeaflet: false });
            return false;
        } else {
            this.setState({ isAlreadyInLeaflet: true, visibleTwoBtns: true, isActiveMainButton: true });
            return true;
        }
    }

    componentWillMount() {
        this.checkIfAlreadyInLeaflet();
        this.setState({ i: this.props.changed });
        if (this.state.isAlreadyInLeaflet) {
            this.setState({ isActiveMainButton: true, visibleTwoBtns: true });
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.changed != this.props.changed && !this.state.isAlreadyInLeaflet) {
            this.setState({ isActiveMainButton: false, visibleTwoBtns: false });
        }
    }

    closeModal = () => {
        this.setState({ modalVisible: false })
    }
    render() {
        // {console.log('this.props ' + this.props.page)}
        if (LeafletsModule)
            return (
                <View style={styles.floatingButtonsHolder}>
                    <TouchableOpacity onPress={() => this.setState({ visibleTwoBtns: !this.state.visibleTwoBtns, isActiveMainButton: !this.state.isActiveMainButton })}><Image style={styles.floatBtnAdd} source={!this.state.isActiveMainButton ? require('./ico/add/add.png') : require('./ico/add/add_close_pressed.png')} /></TouchableOpacity>

                    {this.state.visibleTwoBtns &&
                        <View>
                            <TouchableOpacity onPress={() => this.addToLeaflet()} style={styles.add_leaflet}><Image style={styles.floatBtnAdd} source={!this.state.isAlreadyInLeaflet ? require('./ico/add/add_leaflet.png') : require('./ico/add/add_leafletPressed.png')} /></TouchableOpacity>
                            {PresentationModule && <TouchableOpacity onPress={() => {
                                // this.setState({ modalVisible: !this.state.modalVisible });
                                Actions.refresh({ visiblepresentation: true, pagePresentation: this.props.page })
                                console.log('thispropspage', this.props.page)
                            }}><Image style={styles.floatBtnAdd} source={require('./ico/add/add_mypresentation.png')} /></TouchableOpacity> }
                        </View>
                    }
                </View>
            );
        else
            return (
                <View style={styles.floatingButtonsHolder}>
                    <TouchableOpacity
                        onPress={() => Actions.refresh({ visiblepresentation: true, pagePresentation: this.props.page })}>
                        <Image
                            style={styles.floatBtnAdd}
                            source={require('./ico/add/add.png')} />
                    </TouchableOpacity>
                </View>
            )
    }
}

const styles = StyleSheet.create({
    floatingButtonsHolder: {
        position: 'absolute',
        width: 50,
        height: 158,
        zIndex: 49824982789,
        right: 0,
        top: 140
    },
    floatBtnAdd: {
        width: 50,
        height: 50,
    },
    add_leaflet: {
        paddingBottom: 4,
        paddingTop: 4,
    }
});