import React, { Component } from 'react';
import { StyleSheet, TextInput, Text, View, TouchableOpacity, AsyncStorage, Switch, Alert, ScrollView } from 'react-native';
import Modal from "react-native-modal";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import SettingsLanguage from './SettingsLanguage';
import SettingsFailedFiles from './SettingsFailedFiles';
import SettingsUpdate from './SettingsUpdate';
import SettingsLogin from './SettingsLogin';
import SettingsSignUp from './SettingsSignUp';
import SettingsForgotPassword from './SettingsForgotPassword';
import SettingsChangePassword from './SettingsChangePassword';
import SettingsProfile from './SettingsProfile'
import { LanguageModule } from '../../ModuleConfig'

export default class SettingsComponent extends Component {

  state = {
    settings: '',
    settingsText: '',
    loggedIn: false,
    changePassword: true
  };



  renderContent = () => {
    switch (this.state.settings) {
      case 'languages': return <SettingsLanguage />;
      case 'failedFiles': return <SettingsFailedFiles />;
      case 'update': return <SettingsUpdate />;
      case 'profile': return <SettingsProfile />
      case 'login': return <SettingsLogin onChange={() => this.setState({ loggedIn: !this.state.loggedIn, changePassword: !this.state.changePassword, settings: '' })} changeToSignUp={() => this.setState({ settings: 'signup' })} changeToForgotPassword={() => this.setState({ settings: 'forgotPassword' })} />;
      case 'signup': return <SettingsSignUp changeToLogin={() => this.setState({ settings: 'login' })} />;
      case 'forgotPassword': return <SettingsForgotPassword onChange={(a) => this.setState({ settings: a })} />;
      case 'changePassword': return <SettingsChangePassword logout={() => this.logOut()} />;
      case 'imprint': return <View style={{margin: 10}}><Text style={{fontSize: 20}}>{global.projectJson.project.imprintText}</Text></View>;
      case 'terms': return <View style={{margin: 10}}><Text style={{fontSize: 20}}>{global.projectJson.project.termsText}</Text></View>;
      case 'copyright': return <View style={{margin: 10}}><Text style={{fontSize: 20}}>{global.projectJson.project.copyrightText}</Text></View>;
      default: return (<View></View>);
    }
  }

  componentWillMount() {
    AsyncStorage.getItem('@userId')
      .then(res => {
        if (res == null) {
          this.setState({ loggedIn: false, changePassword: false });
        } else {
          this.setState({ loggedIn: true, changePassword: true });
        }
      })
  }

  logOut = () => {
    AsyncStorage.removeItem('@userId');
    AsyncStorage.removeItem('@userToken');
    global.userToken = '';
    this.setState({ loggedIn: false, changePassword: false, settings: '' });
  }

  loginOrLogout = () => {
    if(this.state.loggedIn) {
      return <TouchableOpacity style={styles.btn_settings} onPress={() => this.logOut()}><Text style={styles.btn_text}>Logout</Text></TouchableOpacity>
    } else {
      return <TouchableOpacity style={[styles.btn_settings, this.state.settings === 'login' ? styles.btn_settings_pressed : '']}   onPress={() => this.setState({ settings: 'login' })}><Text style={styles.btn_text}>Login</Text></TouchableOpacity>
    }
  }


  render() {

    return (

      <View style={styles.content}>
        <View style={styles.buttonsCont}>
          <TouchableOpacity style={[styles.btn_settings, styles.btn_version]} onPress={() => this.setState({ settingsText: global.projectJson.project.version })} disabled><Text style={styles.btn_version_text}>Version: {global.projectJson.project.version}</Text></TouchableOpacity>
          {LanguageModule && <TouchableOpacity style={[styles.btn_settings, this.state.settings === 'languages' ? styles.btn_settings_pressed : '']} onPress={() => this.setState({ settings: 'languages' })}><Text style={styles.btn_text}>Languages</Text></TouchableOpacity>}
          <TouchableOpacity style={[styles.btn_settings, this.state.settings === 'update' ? styles.btn_settings_pressed : '']}  onPress={() => this.setState({ settings: 'update' })}><Text style={styles.btn_text}>Update</Text></TouchableOpacity>
          <TouchableOpacity style={[styles.btn_settings, this.state.settings === 'failedFiles' ? styles.btn_settings_pressed : '']}  onPress={() => this.setState({ settings: 'failedFiles' })}><Text style={styles.btn_text}>Failed Files</Text></TouchableOpacity>
          {this.loginOrLogout()}
          {this.state.changePassword && <TouchableOpacity style={[styles.btn_settings, this.state.settings === 'profile' ? styles.btn_settings_pressed : '']}  onPress={() => this.setState({ settings: 'profile' })}><Text style={styles.btn_text}>Profile</Text></TouchableOpacity>}
          {this.state.changePassword && <TouchableOpacity style={[styles.btn_settings, this.state.settings === 'changePassword' ? styles.btn_settings_pressed : '']}  onPress={() => this.setState({ settings: 'changePassword' })}><Text style={styles.btn_text}>Change Password</Text></TouchableOpacity>}
          {/* <TouchableOpacity style={[styles.btn_settings, this.state.settings === 'imprint' ? styles.btn_settings_pressed : '']}  onPress={() => this.setState({ settings: 'imprint' })}><Text style={styles.btn_text}>Imprint</Text></TouchableOpacity>
          <TouchableOpacity style={[styles.btn_settings, this.state.settings === 'terms' ? styles.btn_settings_pressed : '']}  onPress={() => this.setState({ settings: 'terms' })}><Text style={styles.btn_text}>Terms & Conditions</Text></TouchableOpacity>
          <TouchableOpacity style={[styles.btn_settings, this.state.settings === 'copyright' ? styles.btn_settings_pressed : '']}  onPress={() => this.setState({ settings: 'copyright' })}><Text style={styles.btn_text}>Copyright</Text></TouchableOpacity> */}
        </View>
        <View style={styles.renderContent}>
          {this.renderContent()}
        </View>


      </View>

    );
  }
}
const styles = StyleSheet.create({
  btn_settings: {
    backgroundColor: '#fff',
    height: "8%",
    width: "100%",
    borderWidth: 1,
    borderColor: '#dddddd',
    justifyContent: 'center',
    marginTop: 10,
    alignItems: 'center',
  },
  btn_settings_pressed: {
    borderWidth: 1.5,
    borderColor: 'red',
  },
  btn_text: {
    color: '#757575',
    fontSize: 25
  },
  content: {
    backgroundColor: 'white',
    width: '100%',
    height: '93%',
    flexDirection: 'row',
    position: 'absolute',
    top: '7%',
    zIndex: 3,
  },
  buttonsCont: { 
    backgroundColor: 'white', 
    width: '40%', 
    height: "100%", 
    justifyContent: 'flex-start', 
    alignItems: 'center',
  },
  btn_version: {
    backgroundColor: '#cccccc',
  },
  btn_version_text: {
    color: '#616161',
    fontSize: 25
  },
  renderContent: {
    backgroundColor: '#fff',
    width: '60%', height: "100%",
    borderLeftWidth: 1,
    borderLeftColor: '#dddddd'
  },
});


