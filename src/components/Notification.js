import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image, TouchableWithoutFeedback, ScrollView, ActivityIndicator } from 'react-native';
import moment from 'moment';
import { jsonLogic } from '../../helpers';

export default class NotificationComponent extends Component {

  state = {
    syncer: false
  }  

  renderOneNotification = () => {
    return global.notificationsJson.notifications.map((n, i) => {

      if (n.languageId == global.languageId || n.languageId == 0) {
        return (
          <View key={i} style={styles.single_notification_holder}>
            <View style={styles.single_notification}>
              <View style={styles.wraper}>
                <Text style={styles.textDate}>{moment(Number(n.senddate) * 1000).format('L')}</Text>
                <Text style={styles.textTitle}>{n.title}</Text>
                <Text style={styles.textNotificationContent}>{n.text}</Text>
              </View>
            </View>
          </View>
        );
      }
    })
  }

  reload = () => {
    this.setState({ syncer: true })
    return new Promise((resolve, reject) => {
      jsonLogic('notificationsJson')
        .then(() => resolve())
    })
  }

  syncOrReload = () => {
    if(this.state.syncer) {
      return <ActivityIndicator size='large' />
    } else {
      return <TouchableOpacity onPress={() => this.reload().then(() => { this.setState({ syncer: false }); this.forceUpdate(); })}><Image style={styles.icoClose} source={require('./ico/x64/reload.png')} /></TouchableOpacity>
    }
  }

  render() {


    return (

      <View style={styles.sidebar}>
        <View style={styles.notificationsLayout}>
          <View style={styles.notifications_heading} onPress={this.Version}>
            <View style={{ width: '80%', }}></View>
            <View style={styles.closeReloadCont}>
              {this.syncOrReload()}
              <TouchableOpacity onPress={() => this.props.close()}><Image style={styles.icoClose} source={require('./ico/x64/n-close.png')} /></TouchableOpacity>
            </View>
          </View>
          <ScrollView>
            {this.renderOneNotification()}
          </ScrollView>
        </View>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  single_notification_holder: {
    flexDirection: 'row',
  },
  single_notification: {
    marginTop: 10,
    height: 150,
    width: '100%',
    backgroundColor: '#ccc',
    borderBottomWidth: 1,
    borderBottomColor: '#424242'
  },
  wraper: {
    padding: 30
  },
  textDate: {
    color: '#424242',
  },
  textTitle: {
    paddingTop: 5,
    color: 'black',
    fontSize: 18
  },
  textNotificationContent: {
    paddingTop: 5,
    color: '#212121',
    textAlign: 'left'
  },
  sidebar: {
    backgroundColor: '#ccc',
    width: '40%',
    height: '93%',
    flexDirection: 'row',
    position: 'absolute',
    top: '7%',
    zIndex: 3,
    right: 0
  },
  notificationsLayout: {
    backgroundColor: '#ccc',
    width: '100%',
    height: "100%",
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  notifications_heading: {
    backgroundColor: '#ccc',
    height: "10%",
    width: "100%",
    borderBottomWidth: 1,
    borderColor: '#212121',
    justifyContent: 'center',
    marginTop: 0,
    alignItems: 'center',
    flexDirection: 'row'
  },
  closeReloadCont: {
    width: '20%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center'
  },
  icoClose: {
    height: 21,
    width: 21,
    marginLeft: 10,
    marginRight: 10
  }
});